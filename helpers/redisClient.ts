/**
 * Redis client connection
 */
import config from '../config'

import Redis from 'ioredis'

export default new Redis({
  port: config.redis_port,
  password: config.redis_secret
})