import Redis from 'ioredis'
import config from '../config'

const client = new Redis({
  port: config.redis_port,
  password: config.redis_secret,
})
const subscriber = new Redis({
  port: config.redis_port,
  password: config.redis_secret,
})

export default {
  createClient: function (type: string): Redis.Redis | Redis.Cluster {
    switch (type) {
    case 'client':
      return client
    case 'subscriber':
      return subscriber
    case 'bclient':
      return new Redis({
        port: config.redis_port,
        password: config.redis_secret,
      })
    default:
      throw new Error('Unexpected connection type: ')
    }
  },
}
