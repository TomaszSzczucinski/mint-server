import nodemailer from 'nodemailer'
import config from '../config'

export default nodemailer.createTransport({
  host: 'smtp.domain.com',
  port: 465,
  auth: {
    user: 'dev@mynth.io',
    pass: config.mail_pass,
  },
})