/**
 * ErrorError
 *
 * Handle server errors like boss
 */
import BaseError from './baseError'

export default class ErrorError extends BaseError {
  public readonly redirect?: string

  constructor(
    name: string,
    httpCode = 500,
    message = 'Ah, jakiś problemik się wkradł.',
    redirect?: string
  ) {
    super(name, httpCode, message)
    this.redirect = redirect || undefined
  }
}
