/**
 * Base Error
 * Extend Error class
 */
export default class BaseError extends Error {
  public readonly name: string
  public readonly httpCode: number

  constructor(
    name: string,
    httpCode: number,
    message: string,
  ) {
    super(message)
    Object.setPrototypeOf(this, new.target.prototype)

    this.name = name
    this.httpCode = httpCode

    if (typeof Error.captureStackTrace === 'function') {
      Error.captureStackTrace(this, this.constructor)
    } else {
      this.stack = new Error(name).stack
    }
  }
}