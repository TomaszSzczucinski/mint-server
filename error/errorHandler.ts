/**
 * Error handler
 * Handles error from middleware
 */
import { logger } from './../logging/winston'
import { Response } from 'express'
import ErrorError from './errorError'

class ErrorHandler {
  public async handleError(err: ErrorError, res: Response): Promise<void> {
    // Send response with error
    res
      .status(err.httpCode || 500)
      .json({ type: 'error', message: err.message })
    err.message = ''

    // And log the error
    logger.error('ErrorHandler', err)

    // TODO: await sendMailToAdminIfCritical()
    // TODO: await sendEventsToSentry()
  }

  public isTrustedError(error: Error) {
    if (error instanceof ErrorError) {
      return true
    }
    return false
  }
}

export const errorHandler = new ErrorHandler()
