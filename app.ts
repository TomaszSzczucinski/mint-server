// Imports
import express from 'express'
import ioredis from './helpers/redisClient'
import morgan from 'morgan'
import cors from 'cors'
import helmet from 'helmet'
import xss from 'xss-clean'
import hpp from 'hpp'
import config from './config'
import cookieParser from 'cookie-parser'
import session from 'express-session'
import connectRedis from 'connect-redis'

// Error handler
import { errorHandler } from './error/errorHandler'
import ErrorError from './error/errorError'

// Import Logger
import { logger } from './logging/winston'

// Import routes
import { authRouter } from './api/routes/api.routes.auth'
import { meRouter } from './api/routes/api.routes.me'
import { devRouter } from './api/routes/api.routes.dev'


/**
 *  Don't do anything fancy there.
 *  Just one more tab, no exporting.
 *  Please keep it simple.
 */

/**
 * Catch uncaught exceptions
 */
// ! -----------------------------------------------------
process.on('uncaughtException', (error) => {
  logger.fatal('Uncaught Exception occured', error)
  process.exit(1)
})
// And with Promises
process.on('unhandledRejection', (error, promise) => {
  logger.fatal('Uncaught Promise rejection', promise)
  logger.fatal(' The error was: ', error)
})
// ! -----------------------------------------------------

const RedisStore = connectRedis(session)

// Create express App
const app = express()

// Trust proxy
app.set('trust proxy', true)

// Session with redis store
app.use(
  session({
    secret: config.session_secret,
    store: new RedisStore({
      client: ioredis,
    }),
    saveUninitialized: false,
    resave: false
    // cookie: {
    //   secure: false,
    //   httpOnly: false,
    //   // domain: '127.0.0.1'
    // },
  })
)

// Allow cors
// app.use(cors())
app.use(cors({ origin: true, credentials: true }))

// Add Helmet
app.use(helmet())

// Add Morgan logger
app.use(morgan(config.morgan_format))

// Express JSON
app.use(express.json())

// Xss clean
app.use(xss())

// HPP attacks filter
app.use(hpp())

// Cookie Parser
// TODO change secret
app.use(cookieParser('some_secret_1234'))

/**
 *  Heatlh check
 */
app
  .route(`/${config.api_prefix}`)
  .get(function (req: express.Request, res: express.Response) {
    res.sendStatus(200)
  })
  .head(async function (req: express.Request, res: express.Response) {
    res.sendStatus(200)
  })

/**
 *  API Routes
 */

// Authorization Router
app.use(`/${config.api_prefix}/auth`, authRouter)

// Me Router
app.use(`/${config.api_prefix}/me`, meRouter)

// Blog Router
app.use(`/${config.api_prefix}/dev`, devRouter)

/**
 * Error middleware
 */
app.use(
  async (
    err: ErrorError,
    _req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) => {
    if (!errorHandler.isTrustedError(err)) {
      next(err)
    }
    await errorHandler.handleError(err, res)
  }
)

/**
 *  Start server
 */
app.listen(config.port, () => {
  logger.info(`Listening on: ${config.port}`)
})
