/**
 * Securing accounts,
 * sigin and signup
 */

import Bowser from 'bowser'
import { customAlphabet } from 'nanoid/async'
import { addDays, addMinutes, isBefore } from 'date-fns'
import { Account, AuthRequest, PrismaClient } from '@prisma/client'

// Mynth
import authConfig from '../../config/auth'
import { logger } from '../../logging/winston'
import { removeRefreshTokens } from './services.auth.tokens'
import { emailQueue } from '../../jobs/auth/jobs.auth.shot'

/**
 * Export
 */
export {
  addAuthorizedRequest,
  isAuthorizedRequest,
  updateAuthorizedRequest,
  clearAuthForAccount,
  getAuthRequests,
  unblockAuthRequest,
  removeRequest
}

// Init Prisma client
const prisma = new PrismaClient()

const isAuthorizedRequest = async (
  account: Account,
  authRequest: AuthRequest | null,
  ipAddress: string,
  userAgent: string,
  pin: string | null
): Promise<AuthRequest | null> => {
  // Check conditions, security should be simple and intelligent
  if (
    authRequest &&
    authRequest.validTo &&
    isBefore(new Date(), authRequest.validTo) &&
    !authRequest.blocked
  ) {
    return authRequest
  }

  if (authRequest && pin) {
    if (pin === authRequest.pin) {
      // Parse user agent
      const bowser = Bowser.parse(userAgent)
      const browserName = bowser.browser.name || userAgent.split('/')[0]
      const browserOs = bowser.os.name || ''
      await updateAuthorizedRequest(authRequest.id, ipAddress, browserName, browserOs)
      return authRequest
    }
  }

  // If authentication failed, go with false!
  await newRequestAuthorization(account, authRequest, ipAddress, userAgent)
  return null
}

/**
 * Issue new request authorization
 * @param account Account
 * @param ipAddress IP Addres from Request
 * @param userAgent User Agent from Request
 */
const newRequestAuthorization = async (
  account: Account,
  authRequest: AuthRequest | null,
  ipAddress: string,
  userAgent: string
): Promise<void> => {
  // Have ready for used pin and date
  // Generate pin
  const pin = await customAlphabet('0123456789', 6)()

  // Create valid to date
  const validTo = addMinutes(new Date(), 15)

  // Parse user agent
  const bowser = Bowser.parse(userAgent)
  const browserName = bowser.browser.name || userAgent.split('/')[0]
  const browserOs = bowser.os.name || ''

  // If request is null (so there isn't any)
  // Add new auth request with pinCode
  if (!authRequest) {
    // Add new authorization request
    addAuthorizedRequest(account.id, ipAddress, browserName, browserOs, pin, validTo)
    // Send email
    emailQueue.add(
      { type: 'sendPin', to: account.email, vars: [account.name, pin, browserName, browserOs, ipAddress] },
      {
        attempts: 20,
        backoff: {
          type: 'fixed',
          delay: 5000,
        },
      }
    )
  } else {
    if (!isBefore(new Date(), authRequest.validTo)) {
      // We need to update request with new data
      updateAuthorizedRequest(
        authRequest.id,
        ipAddress,
        browserName,
        browserOs,
        pin,
        validTo
      )

      // Send email
      emailQueue.add(
        { type: 'sendPin', to: account.email, vars: [account.name, pin, browserName, browserOs, ipAddress] },
        {
          attempts: 20,
          backoff: {
            type: 'fixed',
            delay: 5000,
          },
        }
      )
    }
  }
}

/**
 * Add authorized request to DB
 * @param accountId Id of Account
 * @param ipAddress Ip Addres from req
 * @param userAgent User agent from req
 */
const addAuthorizedRequest = async (
  accountId: number,
  ipAddress: string,
  browserName: string,
  browserOs: string,
  pin?: string,
  validTo?: Date
): Promise<AuthRequest> => {

  let blocked = false

  if (pin) blocked = true

  /**
   * Decide for how many dayus request
   * will be authorized and valid,
   * this value won't be refreshed,
   * since ip's are dynamicly assigned.
   */
  const validToAddDays = browserOs
    ? authConfig.validForDays
    : authConfig.undefinedValidForDays

  // If valid to date not passed, create default
  if (!validTo) validTo = addDays(new Date(), validToAddDays)
  console.log(
    '🚀 ~ file: services.auth.secure.ts ~ line 128 ~ validTo',
    validTo
  )

  // Insert into DB
  return await prisma.authRequest.create({
    data: {
      ipAddress: ipAddress,
      browserName: browserName,
      browserOS: browserOs,
      validTo: validTo,
      pin: pin,
      blocked: blocked,
      account: {
        connect: { id: accountId },
      },
    },
  })
}

/**
 *
 * @param id Request ID
 * @param ipAddress IP Address from req
 * @param userAgent User agent from req
 * @param pin Pin code (optional)
 * @param validTo Valid to (optional)
 */
const updateAuthorizedRequest = async (
  id: number,
  ipAddress: string,
  browserName: string,
  browserOs: string,
  pin?: string,
  validTo?: Date
): Promise<void> => {

  let blocked = false

  if (pin) blocked = true

  /**
   * Decide for how many dayus request
   * will be authorized and valid,
   * this value won't be refreshed,
   * since ip's are dynamicly assigned.
   */
  const validToAddDays = browserOs
    ? authConfig.validForDays
    : authConfig.undefinedValidForDays

  // If valid to date not passed, create default
  if (!validTo) validTo = addDays(new Date(), validToAddDays)

  // Insert into DB
  await prisma.authRequest.update({
    where: {
      id: id,
    },
    data: {
      ipAddress: ipAddress,
      browserName: browserName,
      browserOS: browserOs,
      validTo: validTo,
      pin: pin,
      blocked: blocked,
    },
  })
}

/**
 * Clears all auth requests and refresh tokens for given account
 * @param accountId Account ID
 */
const clearAuthForAccount = async (accountId: number): Promise<void> => {
  // Delete all authorized requests and get theirs ids to delete refresh tokens
  const authRequestsIds = await prisma.authRequest.findMany({
    where: {
      accountId: accountId,
    },
    select: {
      id: true,
    },
  })

  // Remove refresh tokens and authRequests
  Promise.all([
    removeRefreshTokens(authRequestsIds),
    removeAuthRequestsForAccount(accountId),
  ]).catch((e) => {
    logger.error(e)
  })
}

/**
 * Helpers
 */

/**
 * Remove all auth Requests for given account
 * @param accountId Account ID
 */
const removeAuthRequestsForAccount = async (accountId: number) => {
  return await prisma.authRequest.deleteMany({
    where: {
      accountId: accountId,
    },
  })
}

/**
 * Get all auth requests for account
 * @param accountId Account ID
 */
const getAuthRequests = async (
  accountId: number
): Promise<
  | {
      id: number
      ipAddress: string
      browserName: string
      browserOS: string
      blocked: boolean
    }[]
  | null
> => {
  return await prisma.authRequest.findMany({
    where: {
      accountId: accountId,
    },
    select: {
      id: true,
      browserName: true,
      browserOS: true,
      ipAddress: true,
      blocked: true,
    },
  })
}

/**
 * Edit Auth Request
 * @param accountId Account ID
 * @param requestId Request ID
 * @param blocked Boolean - block or unblock request
 */
const unblockAuthRequest = async (accountId: number, requestId: number): Promise<void> => {
  await prisma.account.update({
    where: {
      id: accountId,
    },
    data: {
      authRequest: {
        update: {
          where: {
            id: requestId
          },
          data: {
            blocked: false,
            pin: null,
            validTo: addDays(new Date(), authConfig.validForDays)
          },
        }
      }
    }
  })
}

/**
 * Remove Auth Request
 * @param accountId Account ID
 * @param requestId Request ID
 */
const removeRequest = async (accountId: number, requestId: number): Promise<boolean> => {
  const result = await prisma.authRequest.deleteMany({
    where: {
      id: requestId,
      accountId: accountId
    }
  })

  return result.count !== 0
}
