/**
 * Mint user services
 *
 * Handle operations connected with user
 */
import * as argon2 from 'argon2'
import { nanoid } from 'nanoid/async'
import Bowser from 'bowser'
import { Account, PrismaClient, AuthRequest, Profile, Prisma } from '@prisma/client'
import redisClient from '../../helpers/redisClient'
import {
  addDays,
  addMilliseconds,
  addMinutes,
  formatRelative,
  isBefore,
} from 'date-fns'
import { pl } from 'date-fns/locale'
import { RateLimiterRedis } from 'rate-limiter-flexible'
import ErrorError from '../../error/errorError'
import { logger } from '../../logging/winston'
import authConfig from '../../config/auth'
import emailQueue from '../../jobs/auth/jobs.auth.emails'
/**
 * Authentication services for single user
 *
 * Handles:
 *  - Register user - create user
 *  - Make a profile for user - create profile
 *  - Login - login
 */

// * Export functions
export {
  createAccount,
  doesAccountExists,
  authenticateAccount,
  addResetPasswordToken,
  validateResetPasswordToken,
  updatePasswordWithWipe,
  updatePassword,
  removeAccount,
  getAccountPassword,
}

const limiterConsecutiveFailsByEmailAndIP = new RateLimiterRedis({
  storeClient: redisClient,
  keyPrefix: 'login_fail_consecutive_username',
  points: 5,
  duration: 60 * 60 * 24 * 90,
  blockDuration: 60 * 60,
})

// Init Prisma client
const prisma = new PrismaClient()

/**
 * Function will check if email account
 * with that email address exists
 * @param email Email address to check
 */
async function doesAccountExists(email: string): Promise<boolean> {
  // Search for account with that email
  return await prisma.account
    .findUnique({
      where: {
        email: email,
      },
      select: {
        email: true,
      },
    })
    .then((result) => {
      // Return false if email isn't in DB
      if (!result) return false
      return false
    })
    .catch(() => {
      throw new ErrorError(
        'Database',
        500,
        'Coś poszło nie tak jak chcieliśmy. Spróbuj jeszcze raz. I... wybacz nam.'
      )
    })
}

/**
 * Create User account and save him into database
 *
 * @param name User full name
 * @param email User email
 * @param password User password
 */
async function createAccount(
  name: string,
  email: string,
  password: string,
  ipAddress: string,
  userAgent: string
): Promise<Account & {authRequest: {id: number}[]}> {
  // Parse user agent
  const bowser = Bowser.parse(userAgent)
  const browserName = bowser.browser.name || userAgent.split('/')[0]
  const browserOs = bowser.os.name || ''

  // ? Check if bot???

  // Hash password
  const hashedPassword = await argon2.hash(password, {
    type: argon2.argon2id,
    saltLength: 32,
    timeCost: 5,
  })

  const slug = await getProfileSlug(name)  

  // Create account
  return await prisma.account
    .create({
      data: {
        name: name,
        email: email,
        password: hashedPassword,
        role: 'Person',
        authRequest: {
          create: {
            ipAddress: ipAddress,
            browserName: browserName,
            browserOS: browserOs,
            pin: null,
            blocked: false,
            validTo: addDays(new Date(), authConfig.validForDays),
          },
        },
        profile: {
          create: {
            name: name,
            slug: slug
          }
        },
      },
      include: {
        authRequest: {
          select: {
            id: true,
          },
        },
      },
    })
    .then(async (account) => {
      // Send email
      emailQueue.add(
        { type: 'welcome', opts: { to: account.email, name: account.name } },
        {
          attempts: 10,
          backoff: {
            type: 'fixed',
            delay: 5000,
          },
        }
      )

      return account
    })
    .catch((err) => {
      // If email exists in DB
      // TODO Can be shorter???
      if (err.meta.target[0] === 'email') {
        throw new ErrorError('Account', 404, 'Taki email już istnieje!')
      }
      throw new ErrorError(
        'Account',
        500,
        'Niestety nie można się rejestrować. Na pewno już to naprawiamy! :).'
      )
    })
}

/**
 * Authenticate account and return
 * account without password
 *
 * @param email Email
 * @param password Unhashed Password
 * @param conn RethinkDB Connection
 */
const authenticateAccount = async (
  email: string,
  password: string,
  ipAddress: string,
  userAgent: string
): Promise<Account & { authRequest: AuthRequest[] }> => {
  /**
   * Securing from bruteforce attack
   * IP Protection will be implemented
   * in nginx, but here we are securing
   * from same email too many failed
   * attempts
   */

  // Get Email_IP Key
  const emailIpKey = await getEmailIPKey(email, ipAddress)
  // Get limiter info on this email_ip
  const rlResUsername = await limiterConsecutiveFailsByEmailAndIP
    .get(emailIpKey)
    .catch(() => {
      throw new ErrorError(
        'Limit',
        500,
        'Przykro mi, ale teraz nie uda się zalogować. Na pewno już to naprawiamy! :).'
      )
    })

  if (rlResUsername !== null && rlResUsername.consumedPoints > 5) {
    // Log IP Address
    logger.info(`Limitter on IP Address: ${ipAddress}`)
    // Retry in format
    const retryIn = formatRelative(
      addMilliseconds(new Date(), rlResUsername.msBeforeNext),
      new Date(),
      { locale: pl }
    )

    // Throw Error with message
    throw new ErrorError(
      'FlexLimiter.Login.TooManyBad',
      429,
      `To konto doświadczyło kilku nie udanych prób logowania. Kolejny raz możesz spróbować ${retryIn}. Jeśli to nie twoje konto i próbujesz się włamać, to jesteś nie fajny :(`
    )
  }

  // Parse user agent
  const bowser = Bowser.parse(userAgent)
  const browserName = bowser.browser.name || userAgent.split('/')[0]
  const browserOs = bowser.os.name || ''

  const account = await prisma.account
    .findUnique({
      where: {
        email: email,
      },
      include: {
        authRequest: {
          where: {
            ipAddress: ipAddress,
            browserName: browserName,
            browserOS: browserOs,
          },
        },
      },
    })
    .catch(() => {
      throw new ErrorError('Get Account For Login', 500, 'Baza dancyh error.')
    })

  if (!account) {
    throw new ErrorError('No account', 400, 'Podane dane nie są prawidłowe.')
  }

  // Check if password match
  const isPasswordMatch = await argon2
    .verify(account.password, password)
    .catch(() => {
      // Error with argon
      // Not if passwords doesn't match
      throw new ErrorError(
        'Argon',
        500,
        'Ajj. Coś się podzieło, ale zaraz będzie działać. Pssst... Twoje dane zawsze są tutaj bezpieczne.'
      )
    })

  // Check if password doesn't match
  if (!isPasswordMatch) {
    // Attempt counter for IP Address will be implemented with nginx
    await limiterConsecutiveFailsByEmailAndIP
      .consume(emailIpKey)
      .catch((err) => {
        console.log(err)
        if (err instanceof Error) {
          throw new ErrorError(
            'Flex Limiter',
            500,
            'Ahh. Problemy z logowaniem. Nie lubimy tego tak samo jak ty, ale czasem wolimy poczekać, żeby mieć pewność, że jesteś bezpieczny :)'
          )
        }

        /**
         * TODO send email to user
         * Inform him abbout this attempts
         * and allow him to unlock this
         *
         * Planned time to do that:
         *  - After closing up account creation,
         *    login and tokens
         */

        // Format retry in
        const retryIn = formatRelative(
          addMilliseconds(new Date(), err.msBeforeNext),
          new Date(),
          { locale: pl }
        )

        // Password attempts limit
        throw new ErrorError(
          'Login Attempt Limiter',
          429,
          `To nadal nie to hasło. Niestety, kolejna próba ${retryIn}.`
        )
      })

    // Data is invalid
    throw new ErrorError(
      'Verify Password',
      401,
      'Podane dane nie są prawidłowe.'
    )
  }

  // We don't need password anymore
  account.password = ''

  if (rlResUsername !== null && rlResUsername.consumedPoints > 0) {
    await limiterConsecutiveFailsByEmailAndIP.delete(emailIpKey).catch((err) => {
      logger.warn('Limiter delete()', err)
    })
  }

  // Return account if successfully authenticated
  return account
}

/**
 * Create password reset token and update account record,
 * returns token to be send by email to issuer
 * @param email Email
 */
const addResetPasswordToken = async (email: string): Promise<string[]> => {
  // Generate Token
  const token = await nanoid(30)

  // Update account with token and expire time
  return await prisma.account
    .update({
      where: {
        email: email,
      },
      data: {
        resetPassToken: token,
        resetPassExpire: addMinutes(
          new Date(),
          authConfig.resetTokenExpireTime
        ),
      },
      select: {
        name: true,
      },
    })
    .then((name) => {
      return [name.name, token]
    })
    .catch((e) => {
      logger.error(e)
      throw new ErrorError(
        'Issue password reset',
        400,
        'Coś jest nie tak, sprawdź email i spróbuj ponownie'
      )
    })
}

/**
 * Check if token is ok, and return account if exists
 * @param token Reset password token from user
 */
const validateResetPasswordToken = async (token: string): Promise<number> => {
  // Get account by searching for uniq token
  const account = await prisma.account.findUnique({
    where: {
      resetPassToken: token,
    },
    select: {
      id: true,
      resetPassExpire: true,
    },
  })
  // Check if token is valid and not expires
  if (
    account &&
    account.resetPassExpire &&
    isBefore(new Date(), account.resetPassExpire)
  )
    // It's ok return account id
    return account.id
  // If sometinhg wrong return null
  else
    throw new ErrorError(
      'Validate Reset Password Token',
      401,
      'Niestety ten link już nie działa lub jest nie prawidłowy'
    )
}

/**
 * Update Password with wipe of ARequests
 * @param accountId Account ID
 * @param password New password
 * @param ipAddress IP Address from req
 * @param userAgent User agent from req
 */
const updatePasswordWithWipe = async (
  accountId: number,
  password: string,
  ipAddress: string,
  userAgent: string
): Promise<void> => {
  // Parse user agent
  const bowser = Bowser.parse(userAgent)
  const browserName = bowser.browser.name || userAgent.split('/')[0]
  const browserOs = bowser.os.name || ''

  // Hash password
  const hashedPassword = await argon2.hash(password, {
    type: argon2.argon2id,
    saltLength: 32,
    timeCost: 5,
  })

  // Update account record
  // Create auth request, because all were cleaned
  await prisma.account.update({
    where: {
      id: accountId,
    },
    data: {
      password: hashedPassword,
      resetPassToken: null,
      resetPassExpire: new Date(),
      authRequest: {
        create: {
          ipAddress: ipAddress,
          browserName: browserName,
          browserOS: browserOs,
          pin: null,
          blocked: false,
          validTo: addDays(new Date(), authConfig.validForDays),
        },
      },
    },
  })
}

/**
 * Just Update Password
 * @param accountId Account ID
 * @param password New password
 */
const updatePassword = async (
  accountId: number,
  password: string
): Promise<void> => {
  // Hash password
  const hashedPassword = await argon2.hash(password, {
    type: argon2.argon2id,
    saltLength: 32,
    timeCost: 5,
  })

  await prisma.account.update({
    where: {
      id: accountId,
    },
    data: {
      password: hashedPassword,
    },
  })
}

/**
 * Remove account and wipe all data connected with this account
 * @param accountId ID of account
 */
const removeAccount = async (accountId: number): Promise<void> => {
  await prisma.authRequest.deleteMany({
    where: {
      accountId: accountId,
    },
  })

  await prisma.profile.delete({
    where: {
      id: accountId
    },
  })

  await prisma.account.delete({
    where: {
      id: accountId,
    },
  })
}

/** ========================
 *  Helpers
 *  ========================
 */

/**
 * Get account password
 * @param accountId Account id
 */
const getAccountPassword = async (accountId: number): Promise<string> => {
  const account = await prisma.account.findUnique({
    where: {
      id: accountId,
    },
    select: {
      password: true,
    },
  })

  if (!account) throw new Error('No account')

  return account.password
}

/**
 * Create pair - email and ipaddress
 * @param email Email address from body
 * @param ipAddress IP Address from req
 */
const getEmailIPKey = async (
  email: string,
  ipAddress: string
): Promise<string> => {
  return `${email}_${ipAddress}`
}

const getProfileSlug = async(name: string): Promise<string> => {
  let slug = name
    .toLowerCase()
    .replace(/[^a-zA-Z0-9]+/g, '-')
    .replace(/^-+/, '')
    .replace(/-+$/, '')

  // Count names
  const slugCount = await prisma.account.count({
    where: {
      name: name,
    },
  })

  if (slugCount && slugCount > 0) slug = `${slug}-${slugCount + 1}`
  return slug
}