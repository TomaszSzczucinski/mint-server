/**
 * Authorization tokens services
 */

import jwt from 'jsonwebtoken'
import config from '../../config'
import { Account } from '@prisma/client'
import ErrorError from '../../error/errorError'
import ioredis from '../../helpers/redisClient'
import { Request } from 'express'
import { updateAuthorizedRequest } from '../../services/auth/services.auth.secure'
import { logger } from '../../logging/winston'
import Bowser from 'bowser'

// * Exports modules
export {
  generateAccessToken,
  generateRefreshToken,
  authorizeRefreshToken,
  extractAccessToken,
  removeRefreshTokens,
}

/**
 * Returns signed access token
 * Actual data returned:
 *  - account ID
 *  - Account roles
 *  - Profile ID
 * @param account Account with profile id
 */
const generateAccessToken = async (
  account: Account
): Promise<string> => {

  try {
    // Create signed token
    const accessToken = jwt.sign(
      {
        ai: account.id,
        rl: account.role,
        ae: account.email
      },
      config.jwt_acess_token_secret,
      { expiresIn: config.jwt_access_token_life }
    )

    // Return Access Token
    return accessToken
  } catch (err) {
    throw new ErrorError(err, 500)
  }
}

/**
 * Returns signed refresh token
 * Actual data returned
 *  - Issuer Account ID
 *  - Issuer IP Address
 *  - Issuer User Agent
 * @param account Account
 * @param ipAddress Issuer IP Address
 * @param userAgent Issuer User Agent
 */
const generateRefreshToken = async (
  account: Account,
  authRequestId: number,
  ipAddress: string,
  userAgent: string
): Promise<string> => {
  try {
    // Return signed JWT token
    const refreshToken = jwt.sign(
      {
        ai: account.id,
        ip: ipAddress,
        ua: userAgent,
        ri: authRequestId,
      },
      config.jwt_refresh_token_secret,
      { expiresIn: config.jwt_refresh_token_life }
    )

    // Generate key for redis
    const rtk = await getRTK(authRequestId)
    await ioredis.set(rtk, JSON.stringify({ ip: ipAddress, bd: false }))

    // Send token back
    return refreshToken
  } catch (err) {
    console.log(err)
    // Throw error
    throw new ErrorError(
      'Generate Refresh Token',
      500,
      ':| Emm, Mayday, coś się podziało. Prosimy o cierpliwość, zaraz wracamy'
    )
  }
}

/**
 * Validate refresh token, validate issuer data
 * update token if needed
 * @param refreshToken Signed Refresh Token
 * @param ipAddress Issuer Ip Address from req
 * @param userAgent Issuer User Agent from req
 */
const authorizeRefreshToken = async (
  refreshToken: string | null,
  ipAddress: string,
  userAgent: string
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
): Promise<any> => {
  // Return signed JWT token
  if (!refreshToken) throw new ErrorError('No Refresh Token', 401)
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const data: any = jwt.verify(
    refreshToken,
    `${process.env.REFRESH_TOKEN_SECRET}`
  )

  // Get Key for refresh token
  const rtk = await getRTK(data.ri)
  // Get refrsh token info from redis
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const rtv: any = await ioredis.get(rtk).then((str) => {
    if (str) return JSON.parse(str)
  })

  // Is token blocked???
  if (!rtv || rtv.isb)
    throw new ErrorError(
      'ValidateRefreshToken',
      401,
      'Dostęp został zablokowany'
    )

  // TODO Implement user agent checking or no??? We have refresh token in session secured for browser
  if (ipAddress !== rtv.ip) {
    logger.info(ipAddress)
    logger.info(rtv.ip)
    // Parse user agent
    const bowser = Bowser.parse(userAgent)
    const browserName = bowser.browser.name || userAgent.split('/')[0]
    const browserOs = bowser.os.name || ''
    updateAuthorizedRequest(data.ri, ipAddress, browserName, browserOs)
    data.newRefreshToken = true
  }
  // Return token data
  return data
}

/**
 * Extract Access Token from Header
 * Token needs to be in format Bearer <TOKEN>
 * @param req Request
 */
const extractAccessToken = async (req: Request): Promise<string> => {
  console.log(req.signedCookies)
  if (
    req.signedCookies &&
    req.signedCookies.at
  ) {
    return req.signedCookies.at
  }
  return ''
}

/**
 * Remove refresh token from DB
 * @param id Refresh Token id
 */
const removeRefreshToken = async (id: number): Promise<void> => {
  // Get Key for refresh token
  const rtk = await getRTK(id)
  // Remove refresh token
  await ioredis.del(rtk).catch((e) => {
    logger.error('Remove refresh token from redis', e)
  })
}

/**
 * Remove more than one refresh token from DB
 * @param ids Refresh tokens ids array in format [{ id: number }, ...]
 */
const removeRefreshTokens = async (ids: { id: number }[]): Promise<void> => {
  for (let index = 0; index < ids.length; index++) {
    // Remove single token
    removeRefreshToken(ids[index].id)
  }
}

/**
 * Helpers
 */

/**
 * Returns rtk key to redis DB
 * @param id Refresh token ID == Auth Request ID
 */
const getRTK = async (id: number): Promise<string> => {
  return `rtk:${id}`
}
