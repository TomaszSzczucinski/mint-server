/**
 * Mynth
 *
 * Blog helper services
 */

import { PrismaClient, Post } from '@prisma/client'
import { logger } from '../../logging/winston'

// Mynth config
import devConfig from '../../config/dev'

// Init Prisma client
const prisma = new PrismaClient()

// Exports
export { getPosts, getPostBySlug, like, unLike }

/**
 * Get list of posts
 *
 * TODO Implement filters and pagination
 *
 * @returns List of posts, can be empty
 */
const getPosts = async (
  categories: string[],
  pLangs: string[],
  page: number,
  take: number
): Promise<Post[]> => {
  console.log(pLangs)
  // Get posts
  return await prisma.post
    .findMany({
      where: {
        ...(categories[0]
          ? {
            categories: {
              hasSome: categories,
            },
          }
          : {}),
        ...(pLangs[0]
          ? {
            pLangs: {
              hasSome: pLangs,
            },
          }
          : {}),
      },
      orderBy: {
        createdAt: 'desc',
      },
      skip: 30 * page,
      take: take || devConfig.default_limit
    })
    .catch((e) => {
      // Save error for feature use in debugging
      logger.warn('Get posts, services.dev.blog', e)
      return []
    })
}

/**
 * Get single post by slug
 *
 * @returns Post or null if post not finded
 */
const getPostBySlug = async (slug: string): Promise<Post | null> => {
  // Get posts
  return await prisma.post
    .findUnique({
      where: {
        slug: slug,
      },
    })
    .catch((e) => {
      // Save error for feature use in debugging
      logger.warn('Get posts, services.dev.blog', e)
      return null
    })
}

const like = async (profileId: number, postId: number): Promise<void> => {
  await prisma.post.update({
    where: {
      id: postId,
    },
    data: {
      likes: {
        increment: 1,
      },
      postLike: {
        create: {          
          devProfile: {
            connect: {
              id: profileId,
            },
          },
        },
      },
    },
  })
}

const unLike = async (profileId: number, postId: number): Promise<void> => {
  await prisma.post.update({
    where: {
      id: postId,
    },
    data: {
      likes: {
        decrement: 1,
      },
      postLike: {
        delete: {
          profileId_postId: {
            profileId: profileId,
            postId: postId
          }
        },
      },
    },
  })
}
