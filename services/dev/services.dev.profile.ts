/**
 * Mynth
 *
 * Blog helper services
 */

import { PrismaClient, PostDraft } from '@prisma/client'
import { customAlphabet } from 'nanoid/async'
import devConfig from '../../config/dev'
import { PostUpdateInterface } from '../../interfaces/blog/interfaces.blog.blog'

// Init Prisma client
const prisma = new PrismaClient()

// Exports
export {
  publicProfile,
  combo,
  getDraft,
  editDraft,
  createDraft,
  publishDraft,
  removeDraft,
  createDevProfile,
  removePost,
  checkLike,
  addAvatar,
  draftFromPost,
  addImage,
  addPostToLibrary,
  removePostFromLibrary,
}

const createDevProfile = async (profileId: number): Promise<any> => {
  return await prisma.devProfile.create({
    data: {
      id: profileId,
    },
    select: {
      createdAt: true,
      post: {
        select: {
          id: true,
          title: true,
          slug: true,
          tags: true,
          likes: true,
        },
        orderBy: {
          createdAt: 'desc',
        },
      },
      postDraft: {
        select: {
          id: true,
          title: true,
          tags: true,
          createdAt: true,
        },
        orderBy: {
          createdAt: 'desc',
        },
      },
      postLike: true,
      notification: true,
      library: true,
    },
  })
}

/**
 * Get combo for profile
 *
 * TODO Define type of return from function
 *
 * @returns Profile with posts and posts drafts summary, and reactions and notifications
 */
const publicProfile = async (slug: string): Promise<any> => {
  if (!slug) return null
  // Create draft and return ID
  return await prisma.profile.findUnique({
    where: {
      slug: slug,
    },
    select: {
      name: true,
      slug: true,
      avatar: true,
      createdAt: true,
    },
  })
}

/**
 * Get combo for profile
 *
 * TODO Define type of return from function
 *
 * @returns Profile with posts and posts drafts summary, and reactions and notifications
 */
const combo = async (profileId: number): Promise<any> => {
  // Create draft and return ID
  return await prisma.devProfile.findUnique({
    where: {
      id: profileId,
    },
    select: {
      createdAt: true,
      post: {
        select: {
          id: true,
          title: true,
          slug: true,
          tags: true,
          likes: true,
        },
        orderBy: {
          createdAt: 'desc',
        },
      },
      postDraft: {
        select: {
          id: true,
          title: true,
          tags: true,
          createdAt: true,
        },
        orderBy: {
          createdAt: 'desc',
        },
      },
      postLike: true,
      notification: true,
      library: true,
    },
  })
}

/**
 * Create draft
 *
 * @returns Post Draft
 */
const createDraft = async (profileId: number): Promise<PostDraft> => {
  // Create draft and return ID
  const draft = await prisma.postDraft.create({
    data: {
      title: '',
      devProfile: {
        connect: {
          id: profileId,
        },
      },
    },
  })

  return draft
}

/**
 *
 * POSTS
 *
 */

/**
 * Get draft
 * @param draftId Draft Id
 * @returns Post draft object or null
 */
const getDraft = async (draftId: number): Promise<PostDraft | null> => {
  // Get Post Draft
  return await prisma.postDraft.findUnique({
    where: {
      id: draftId,
    },
  })
}

/**
 * Edit draft
 */
const editDraft = async (
  draftId: number,
  data: PostUpdateInterface
): Promise<boolean> => {
  // Update DB
  const result = await prisma.postDraft.update({
    where: {
      id: draftId,
    },
    data: {
      ...data,
    },
  })

  // Returns true if success
  return result ? true : false
}

/**
 * Publish draft
 */
const publishDraft = async (
  draftId: number,
  profileId: number
): Promise<boolean> => {
  // Get
  const draft = await prisma.postDraft.findFirst({
    where: {
      id: draftId,
      profileId: profileId,
    },
    select: {
      title: true,
      tags: true,
      authors: true,
      hlights: true,
      content: true,
      categories: true,
      pLangs: true,
    },
  })

  if (!draft) return false // No draft found

  // Create slug
  let slug = encodeURIComponent(
    draft.title
      .toLowerCase()
      .replace(/[^a-zA-Z0-9]+/g, '-')
      .replace(/^-+/, '')
      .replace(/-+$/, '')
      .substr(0, 25)
  )
  const slugId = await customAlphabet(
    '1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM',
    8
  )()

  slug = slug + '-' + slugId

  // Upsert Post
  const upsertPost = prisma.post.upsert({
    where: {
      id: draftId,
    },
    create: {
      id: draftId,
      ...draft,
      slug: slug,
      devProfile: {
        connect: {
          id: profileId,
        },
      },
    },
    update: draft,
  })

  // Update draft
  const updateDraft = prisma.postDraft.delete({
    where: {
      id: draftId,
    },
  })

  // Make as transaction
  const [resUpsertPost, resUpdateDraft] = await prisma.$transaction([
    upsertPost,
    updateDraft,
  ])

  if (resUpsertPost && resUpdateDraft) return true
  return false
}

/**
 * Get Reactions
 */
const checkLike = async (
  profileId: number,
  postId: number
): Promise<boolean | null> => {
  // Get
  const like = await prisma.postLike.findUnique({
    where: {
      profileId_postId: {
        profileId: profileId,
        postId: postId,
      },
    },
  })

  if (like) return true
  return false
}

/**
 * Add Avatar to DB
 */
const addAvatar = async (profileId: number): Promise<void> => {
  await prisma.profile.update({
    where: {
      id: profileId,
    },
    data: {
      avatar: devConfig.upload_root + profileId + '/avatar',
    },
  })
}

/**
 * Add Image to DB
 */
const addImage = async (
  profileId: number,
  fileName: string
): Promise<boolean> => {
  const result = await prisma.media.create({
    data: {
      devProfile: {
        connect: {
          id: profileId,
        },
      },
      name: fileName,
      type: 'image',
    },
  })

  return result ? true : false
}

/**
 * Remove draft
 */
const removeDraft = async (draftId: number): Promise<boolean> => {
  const result = await prisma.postDraft.deleteMany({
    where: {
      id: draftId,
    },
  })

  if (result.count !== 0) return true
  return false
}

/**
 * Remove draft
 */
const draftFromPost = async (
  profileId: number,
  postId: number
): Promise<PostDraft | null> => {
  const post = await prisma.devProfile.findUnique({
    where: {
      id: profileId,
    },
    select: {
      post: {
        where: {
          id: postId,
        },
        select: {
          title: true,
          content: true,
          authors: true,
          lang: true,
          pLangs: true,
          category: true,
          categories: true,
          image: true,
          tags: true,
          hlights: true,
        },
      },
    },
  })

  if (!post) return null

  const draft = await prisma.postDraft.create({
    data: {
      id: postId,
      profileId: profileId,
      ...post.post[0],
    },
  })

  if (draft) return draft
  return null
}

/**
 * Remove post
 */
const removePost = async (
  profileId: number,
  postId: number,
  saveDraft: boolean
): Promise<boolean> => {
  return await prisma.post
    .findMany({
      where: {
        id: postId,
        profileId: profileId,
      },
      select: {
        title: true,
        content: true,
        authors: true,
        lang: true,
        pLangs: true,
        category: true,
        categories: true,
        image: true,
        tags: true,
        hlights: true,
      },
    })
    .then(async (result) => {
      if (result) {
        const post = result[0]
        const deletePost = prisma.post.delete({
          where: {
            id: postId,
          },
        })

        if (saveDraft) {
          const updateDraft = prisma.postDraft.create({
            data: {
              ...post,
              devProfile: {
                connect: {
                  id: profileId,
                },
              },
            },
          })

          const [result1, result2] = await prisma.$transaction([
            updateDraft,
            deletePost,
          ])
          if (result1 && result2) return true
        } else {
          const [result] = await prisma.$transaction([deletePost])
          if (result) return true
        }
        return false
      }
      return false
    })
}

/**
 * Add post to library
 */
const addPostToLibrary = async (
  profileId: number,
  postId: number
): Promise<boolean> => {
  // Update DB
  const result = await prisma.library.create({
    data: {
      postId: postId,
      devProfile: {
        connect: {
          id: profileId,
        },
      },
    },
  })

  // Returns true if success
  return result ? true : false
}

/**
 * Remove post from library
 */
const removePostFromLibrary = async (
  profileId: number,
  postId: number
): Promise<boolean> => {
  // Update DB
  const result = await prisma.devProfile.update({
    where: {
      id: profileId,
    },
    data: {
      library: {
        deleteMany: {
          postId: postId,
        },
      },
    },
  })

  // Returns true if success
  return result ? true : false
}
