/**
 * Mynth
 *
 * Blog helper services
 */

import { PrismaClient, Post, PostDraft, Profile } from '@prisma/client'

// Init Prisma client
const prisma = new PrismaClient()

// Exports
export { searchPosts, searchProfiles }

/**
 * Get Reaction
 */
const searchPosts = async (
  searchFor: string,
  pLangs?: string[],
  tags?: string[],
  authors?: string[],
  ai_tags?: string[],
  lang?: string
): Promise<Post[] | null> => {
  const searchForArray = searchFor.toLowerCase().split(' ')
  // Get
  const post = await prisma.post.findMany({
    where: {
      OR: [
        {
          title: {
            startsWith: searchFor,
            mode: 'insensitive',
          },
        },
        {
          title: {
            contains: searchFor,
            mode: 'insensitive',
          },
        },
        {
          tags: {
            hasSome: searchForArray,
          },
        },
        {
          pLangs: {
            hasSome: searchForArray,
          },
        },
      ],
    },
    orderBy: {
      likes: 'desc',
    },
    take: 10,
  })

  return post
}

/**
 * Search for profiles
 */
const searchProfiles = async (searchFor: string): Promise<Profile[] | null> => {
  // Get
  const profile = await prisma.profile.findMany({
    where: {
      OR: [
        {
          name: {
            contains: searchFor,
            mode: 'insensitive',
          },
        },
        {
          slug: {
            contains: searchFor,
            mode: 'insensitive',
          },
        },
      ],
    },
    take: 15,
  })

  return profile
}

/**
 * Search for profiles
 */
const searchMyPosts = async (
  searchFor: string
): Promise<{ myPosts: Post[] | null; myDrafts: PostDraft[] | null }> => {
  const searchForArray = searchFor.toLowerCase().split(' ')

  // Get posts
  const posts = await prisma.post.findMany({
    where: {
      title: {
        contains: searchFor,
        mode: 'insensitive',
      },
    },
    take: 10,
  })

  // Get drafts
  const postDrafts = await prisma.postDraft.findMany({
    where: {
      title: {
        contains: searchFor,
        mode: 'insensitive',
      },
    },
    take: 10,
  })

  return { myPosts: posts, myDrafts: postDrafts }
}
