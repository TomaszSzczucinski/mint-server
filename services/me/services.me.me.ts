/**
 * Mynth
 *
 * Blog helper services
 */

import { PrismaClient, Profile } from '@prisma/client'
import { logger } from '../../logging/winston'

// Mynth config
import devConfig from '../../config/dev'

// Init Prisma client
const prisma = new PrismaClient()

// Exports
export { getProfile }

const getProfile = async (id: number): Promise<Profile | null> => {
  return await prisma.profile.findUnique({
    where: {
      id: id,
    },
  })
}
