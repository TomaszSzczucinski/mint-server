/**
 * Mynth
 *
 * Blog helper services
 */

import { PrismaClient, Profile } from '@prisma/client'
import { logger } from '../../logging/winston'
import avatarQueue from '../../jobs/me/jobs.me.avatar'

// Mynth config
import meConfig from '../../config/me'

// Init Prisma client
const prisma = new PrismaClient()


export const editProfile = async (id: number, data: any): Promise<Profile | null> => {
  return await prisma.profile
    .update({
      where: {
        id: id,
      },
      data: data,
    })
    .then((result) => {
      console.log(result)
      return result
    })
}

export const processAvatar = async (
  id: number,
  filePath: string
): Promise<string> => {
  // Upload directory
  const directory = `${meConfig.upload_root}/${id}/`

  await avatarQueue.add(
    { filePath: filePath, directory: directory },
    {
      attempts: 100,
      backoff: 5000,
    }
  )

  return `${meConfig.media_path}/${id}/avatar`
}