/**
 * Authorization Schemas
 *
 * used for validation with middlewares
 * and other functions
 */
import Joi from 'joi'

export { signIn, email, resetPassword, newPassword, signUp, editAuthRequest }

/**
 * Sign in validation
 */
const signIn = Joi.object({
  email: Joi.string().required().email().messages({
    'any.required': 'Email jest wymagany',
    'string.empty': 'Nie wpisano maila',
  }),
  password: Joi.string().required().messages({
    'any.required': 'Hasło jest wymagane',
    'string.empty': 'Nie wpisano hasła',
  }),
  pin: Joi.string().allow(null, ''),
  meta: Joi.object({
    ifm: Joi.string().uri()
  })
})

const email = Joi.object({
  email: Joi.string().required().email().messages({
    'any.required': 'Email jest wymagany',
    'string.empty': 'Nie wpisano maila',
  })
})

const resetPassword = Joi.object({
  password: Joi.string().required().min(6).messages({
    'any.required': 'Hasło jest wymagane',
    'string.min': 'Hasło musi posiadać co najmniej 6 znaków',
    'string.empty': 'Nie wpisano hasła',
  }),
  token: Joi.string().required().length(30).messages({
    'any.required': 'Nie prawidłowy link',
    'string.empty': 'Nie prawidłowy link',
    'string.length': 'Nie prawidłowy'
  }),
})

const newPassword = Joi.object({
  ap: Joi.string().required().messages({
    'any.required': 'Aktualne hasło jest wymagane',
    'string.empty': 'Nie wpisano hasła',
  }),
  np: Joi.string().required().min(6).messages({
    'any.required': 'Hasło jest wymagane',
    'string.min': 'Hasło musi posiadać co najmniej 6 znaków',
    'string.empty': 'Nie wpisano hasła',
  }),
  so: Joi.boolean().required().messages({
    'any.required': 'Wylogować wszystkich?',
  })
})

/**
 * Sign up validation
 */
const signUp = Joi.object({
  name: Joi.string().min(3).max(55).required().messages({
    'any.required': 'Imię jest wymagane',
    'string.min': 'Imię i nazwisko musi posiadać co najmniej 3 znaki',
    'string.max': 'Imię i nazwisko ma limit znaków do 35',
    'string.empty': 'Nie wpisano imienia i nazwiska',
  }),
  email: Joi.string().required().email().messages({
    'any.required': 'Mail jest wymagany',
    'string.empty': 'Nie wpisano maila',
  }),
  password: Joi.string().required().min(6).messages({
    'any.required': 'Hasło jest wymagane',
    'string.min': 'Hasło musi posiadać co najmniej 6 znaków',
    'string.empty': 'Nie wpisano hasła',
  }),
})

/**
 * Edit auth request
 */
const editAuthRequest = Joi.object({
  blocked: Joi.boolean().required().messages({
    'any.required': 'Brak opcji',
    'string.empty': 'Brak opcji',
  })
})
