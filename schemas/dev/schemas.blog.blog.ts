/**
 * Authorization Schemas
 *
 * used for validation with middlewares
 * and other functions
 */
import Joi from 'joi'

export { createNew, editProfile, editPost, removePost }

/**
 * Create new blog validation
 */
const createNew = Joi.object({
  nm: Joi.string().required().min(3).messages({
    'any.required': 'Tytuł jest wymagany',
    'string.empty': 'Tytuł jest wymagany',
    'string.min': 'Przynajmniej 3 znaki',
  }),
  sg: Joi.string()
    .base64({ urlSafe: true, paddingRequired: false })
    .min(3)
    .messages({
      'string.min': 'Przynajmniej 3 znaki',
      'string.base64': 'Tylko litery, cyfry i "_"',
    }),
})

/**
 * Edit profile validation
 */
const editProfile = Joi.object({
  name: Joi.string().max(30).messages({
    'string.empty': 'Nazwa nie może być pusta',
    'string.max': 'Maks 30 znaków',
  }),
  bio: Joi.string().max(255).messages({
    'string.empty': 'Nazwa nie może być pusta',
    'string.max': 'Maks 255 znaków',
  }),
})

/**
 * Remove post
 */
const removePost = Joi.object({
  saveDraft: Joi.boolean().default(true)
})

/**
 * Edit post validation
 */
const editPost = Joi.object({
  title: Joi.string().max(255).messages({
    'string.empty': 'Brak opisu',
    'string.max': 'Za długi opis',
  }),
  hlights: Joi.object({
    name: Joi.string().max(20),
    data: Joi.string(),
  }),
  tags: Joi.array().items(Joi.string().max(30)),
  categories: Joi.array().items(Joi.string().max(30)),
  pLangs: Joi.array().items(Joi.string().max(30)),
  content: Joi.object({
    rows: Joi.array().required().items(
      Joi.object({
        cols: Joi.array().required().items(
          Joi.object({
            type: Joi.string().required().max(30),
            data: Joi.array().required().items(
              Joi.object({
                data: Joi.string().required(),
                type: Joi.string(),
              })
            ),
          })
        )
      })
    )
  })
})

/**
 * Add block
 */
const addBlock = Joi.object({
  type: Joi.string().max(30).messages({
    'string.empty': '',
    'string.max': '',
  })
})
