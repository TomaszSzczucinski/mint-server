import Joi from 'joi'

/**
 * Edit my profile validation
 */
export const editProfile = Joi.object({
  name: Joi.string().max(50),
  // bio: Joi.string().max(255),
})