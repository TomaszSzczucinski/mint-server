/**
 *
 *  Winston
 *  Create custom configuration for winston logger
 *
 */
/* eslint-disable @typescript-eslint/no-explicit-any */
import winston from 'winston'
import DailyRotateFile from 'winston-daily-rotate-file'
import config from './../config/logger'

// export const logger = winston.createLogger({
//   level: config.log_level,
//   format: winston.format.json(),
//   transports: [
//     new winston.transports.Console(),
//     new DailyRotateFile({
//       filename: config.log_file,
//       dirname: config.log_directory,
//       datePattern: 'YYYY-MM-DD',
//       zippedArchive: true,
//       maxFiles: '14d',
//       maxSize: '20m',
//     }),
//   ],
// })

// Define custom levels
const customLevels = {
  levels: {
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    fatal: 0,
  },
  colors: {
    trace: 'white',
    debug: 'green',
    info: 'cyan',
    warn: 'yellow',
    error: 'red',
    fatal: 'red',
  },
}

// Format output
const formatter = winston.format.combine(
  winston.format.colorize(),
  winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
  winston.format.splat(),
  winston.format.printf((info) => {
    const { timestamp, level, message, ...meta } = info

    return `${timestamp} [${level}] ${message}\n${
      Object.keys(meta).length
        ? meta.stack
        : ''
    }`
  })
)

// Create logger class
class Logger {
  private logger: winston.Logger

  constructor() {
    const prodTransport = new DailyRotateFile({
      filename: config.log_file,
      dirname: config.log_directory,
      datePattern: 'YYYY-MM-DD',
      zippedArchive: true,
      maxFiles: '14d',
      maxSize: '20m',
    })
    const transport = new winston.transports.Console({
      format: formatter,
    })
    this.logger = winston.createLogger({
      level: config.log_level,
      levels: customLevels.levels,
      transports: [transport, prodTransport],
    })
    winston.addColors(customLevels.colors)
  }

  trace(msg: any, meta?: any) {
    this.logger.log('trace', msg, meta)
  }

  debug(msg: any, meta?: any) {
    this.logger.debug(msg, meta)
  }

  info(msg: any, meta?: any) {
    this.logger.info(msg, meta)
  }

  warn(msg: any, meta?: any) {
    this.logger.warn(msg, meta)
  }

  error(msg: any, meta?: any) {
    this.logger.error(msg, meta)
  }

  fatal(msg: any, meta?: any) {
    this.logger.log('fatal', msg, meta)
  }
}

// Export logger
export const logger = new Logger()
