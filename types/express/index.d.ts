/**
 * Express types
 *
 *
 */
declare global {
  declare module 'express-session' {
    interface Session {
      rt: string | null
      ai: number | null
    }
  }
}

declare namespace Express {
  import '@prisma/cli'
  import { Account, AuthRequest } from '@prisma/cli'
  export interface Request {
    token: AccessTokenInterface
    queryPolluted: any
    blogId: number
    account: Account & {
      authRequest: AuthRequest[]
      profile: {
        id: number
      } | null
    }
  }
}
