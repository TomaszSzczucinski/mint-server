/**
 *
 *  Auth.ts
 *
 *  Authorization routes
 *
 */
import { PrismaClient } from '@prisma/client'
import { Router, Request, Response } from 'express'

// Mynth
import * as sign from '../../controllers/auth/controllers.auth.account'
import * as tokens from '../../controllers/auth/controllers.auth.tokens'
import * as secure from '../../controllers/auth/controllers.auth.secure'
import emailQueue from '../../jobs/auth/jobs.auth.emails'
import validateSchema from '../../middlewares/validation/schemaValidation'
import * as schemas from '../../schemas/auth/sign'

// Export Router
export const authRouter = Router()

/**
 * Sign In Route
 *
 * => Validate data
 * => Sign In -> Authorize -> Generate tokens
 * => Return OK/Unauthorized
 */
authRouter.post(
  '/signin',
  validateSchema(schemas.signIn),
  sign.signIn,
  tokens.sendAccessToken
)

/**
 * Sign up Route
 *
 * => Validate data
 * => Sign Up -> Create tokens -> log in
 * => Return OK/Error
 */
authRouter.post(
  '/signup',
  validateSchema(schemas.signUp),
  sign.signUp,
  tokens.sendAccessToken
)

/**
 * Sign out
 *
 * => Remove refresh token from session
 * > (Client app should remove local access token also)
 * => Return OK/Error
 */
authRouter.get('/signout', sign.signOut)

/**
 * Refresh Access Token
 *
 * => Authorize Refresh Token
 * => Respons with Access Token
 * => Return OK/Error
 */
authRouter.get('/auth', tokens.refreshAccessToken, tokens.sendAccessToken)

/**
 * Set new Password
 *
 * => Authorize Access Token
 * => Validate with schema
 * => Set new Password
 * -> * Logout from all devices
 * => Return OK/Error
 */
authRouter.post(
  '/newPassword',
  tokens.authorizeAccessToken,
  validateSchema(schemas.newPassword),
  sign.setNewPassword
)

/**
 * Reset Password
 *
 * => Validate password and token
 * => Reset password
 * -> Validate token
 * -> Validate expire time of token
 * -> Logout from all devices
 * -> Update account with new password
 * => Return OK/Error
 */
authRouter.post(
  '/resetPassword',
  validateSchema(schemas.resetPassword),
  sign.passwordReset
)

/**
 * Issue Password Reset
 *
 * => Validate email
 * => Create password reset request
 * -> Generate token
 * -> Send email with token
 * -> Update account with token and expire time
 * => Return OK/Error
 */
authRouter.post(
  '/issuePasswordReset',
  validateSchema(schemas.email),
  sign.issuePasswordReset
)

authRouter.get(
  '/authRequests',
  tokens.authorizeAccessToken,
  secure.listAuthRequests
)

authRouter.post(
  '/authRequests/:id',
  tokens.authorizeAccessToken,
  secure.unblockAuthRequest
)

authRouter.delete(
  '/authRequests/:id',
  tokens.authorizeAccessToken,
  secure.removeAuthRequest
)
/**
 *
 *
 *  DEV
 *
 *
 *
 */
/************************************************************************************
 *
 * Dev routes
 *
 * Dev helpers
 */
authRouter.get(
  '/accs',
  tokens.authorizeAccessToken,
  secure.allowedFor('God'),
  async (req: Request, res: Response) => {
    const prisma = new PrismaClient()

    await prisma.account
      .findMany({
        include: {
          authRequest: true,
        },
      })
      .then((result) => {
        console.log(result)
        res.status(200).json(result)
      })
  }
)

authRouter.get(
  '/dev',
  tokens.authorizeAccessToken,
  async (req: Request, res: Response) => {
    const prisma = new PrismaClient()

    // Send email
    emailQueue.add(
      {
        type: 'login_pin',
        opts: { to: 'tomaszszczucinski@gmail.com', pin: '123456', browserName: 'Opera', browserOs: 'Windows 10', ipAddress: '192.168.1.100' },
      },
      {
        attempts: 10,
        backoff: {
          type: 'fixed',
          delay: 5000,
        },
      }
    )
    // await prisma.emailTemplate.create({
    //   data: {
    //     type: 'login_pin',
    //     data: '<!DOCTYPE html><html xmlns=http://www.w3.org/1999/xhtml xmlns:o=urn:schemas-microsoft-com:office:office xmlns:v=urn:schemas-microsoft-com:vml><title></title><!--[if !mso]><!-- --><meta content="IE=edge"http-equiv=X-UA-Compatible><!--<![endif]--><meta content="text/html; charset=UTF-8"http-equiv=Content-Type><meta content="width=device-width,initial-scale=1"name=viewport><style>#outlook a{padding:0}.ReadMsgBody{width:100%}.ExternalClass{width:100%}.ExternalClass *{line-height:100%}body{margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%}table,td{border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0}img{border:0;height:auto;line-height:100%;outline:0;text-decoration:none;-ms-interpolation-mode:bicubic}p{display:block;margin:13px 0}</style><!--[if !mso]><!--><style>@media only screen and (max-width:480px){@-ms-viewport{width:320px}@viewport{width:320px}}</style><!--<![endif]--><!--[if mso]><xml><o:officedocumentsettings><o:allowpng><o:pixelsperinch>96</o:pixelsperinch></o:officedocumentsettings></xml><![endif]--><!--[if lte mso 11]><style>.outlook-group-fix{width:100%!important}</style><![endif]--><!--[if !mso]><!--><link href="https://fonts.googleapis.com/css?family=Roboto:400,700"rel=stylesheet><link href="https://fonts.googleapis.com/css?family=Ubuntu:400,700"rel=stylesheet><link href="https://fonts.googleapis.com/css?family=Cabin:400,700"rel=stylesheet><style>@import url(https://fonts.googleapis.com/css?family=Roboto:400,700);@import url(https://fonts.googleapis.com/css?family=Ubuntu:400,700);@import url(https://fonts.googleapis.com/css?family=Cabin:400,700);</style><!--<![endif]--><style>@media only screen and (min-width:480px){.mj-column-per-100{width:100%!important;max-width:100%}}</style><style></style><style>.hide_on_mobile{display:none!important}@media only screen and (min-width:480px){.hide_on_mobile{display:block!important}}.hide_section_on_mobile{display:none!important}@media only screen and (min-width:480px){.hide_section_on_mobile{display:table!important}}.hide_on_desktop{display:block!important}@media only screen and (min-width:480px){.hide_on_desktop{display:none!important}}.hide_section_on_desktop{display:table!important}@media only screen and (min-width:480px){.hide_section_on_desktop{display:none!important}}[owa] .mj-column-per-100{width:100%!important}[owa] .mj-column-per-50{width:50%!important}[owa] .mj-column-per-33{width:33.333333333333336%!important}p{margin:0}@media only print and (min-width:480px){.mj-column-per-100{width:100%!important}.mj-column-per-40{width:40%!important}.mj-column-per-60{width:60%!important}.mj-column-per-50{width:50%!important}mj-column-per-33{width:33.333333333333336%!important}}</style><body style=background-color:#FFF><div style=background-color:#FFF><!--[if mso | IE]><table border=0 cellpadding=0 cellspacing=0 align=center style=width:600px width=600><tr><td style=line-height:0;font-size:0;mso-line-height-rule:exactly><![endif]--><div style="Margin:0 auto;max-width:600px"><table border=0 cellpadding=0 cellspacing=0 role=presentation style=width:100% align=center><tr><td style="direction:ltr;font-size:0;padding:9px 0 9px 0;text-align:center;vertical-align:top"><!--[if mso | IE]><table border=0 cellpadding=0 cellspacing=0 role=presentation><tr><td style=vertical-align:top;width:600px><![endif]--><div style=font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100% class="mj-column-per-100 outlook-group-fix"><table border=0 cellpadding=0 cellspacing=0 role=presentation style=vertical-align:top width=100%><tr><td style="font-size:0;padding:15px 15px 15px 15px;word-break:break-word"align=left><div style=font-family:Ubuntu,Helvetica,Arial,sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000><h1 style=font-family:Cabin,sans-serif;text-align:center><span style=font-family:Ubuntu,sans-serif;font-size:22px>MYNTH</span></h1></div><tr><td style=font-size:0;word-break:break-word><!--[if mso | IE]><table border=0 cellpadding=0 cellspacing=0 role=presentation><tr><td style=vertical-align:top;height:50px height=50><![endif]--><div style=height:50px></div><!--[if mso | IE]><![endif]--><tr><td style="font-size:0;padding:15px 15px 15px 15px;word-break:break-word"align=left><div style=font-family:Ubuntu,Helvetica,Arial,sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000><p style=text-align:center><span style=font-size:20px>PIN</span></div><tr><td style="font-size:0;padding:15px 15px 15px 15px;word-break:break-word"align=left><div style=font-family:Ubuntu,Helvetica,Arial,sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000><p style=text-align:center><span style=font-family:Roboto,Tahoma,sans-serif><span style=font-size:36px><%= pin %></span></span></div><tr><td style="font-size:0;padding:15px 15px 15px 15px;word-break:break-word"align=left><div style=font-family:Ubuntu,Helvetica,Arial,sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000><p style=text-align:center><span style=font-size:18px><% browserName %>, <% browserOs %></span><p style=text-align:center><span style=font-size:18px>IP: <% ipAddress %></span></div></table></div><!--[if mso | IE]><![endif]--></table></div><!--[if mso | IE]><![endif]--></div>',
    //     from: 'mynth@mynth.io',
    //     subject: 'PIN: <%= pin %>, próba logowania na twoje konto mynth.io'
    //   }
    // })

    res.sendStatus(200)
  }
)

authRouter.get(
  '/rmrf',
  tokens.authorizeAccessToken,
  secure.allowedFor('God'),
  async (req: Request, res: Response) => {
    const prisma = new PrismaClient()

    await prisma.profile.deleteMany({
      where: {
        accountId: {
          not: req.token.iid,
        },
      },
    })
    await prisma.authRequest.deleteMany({
      where: {
        accountId: {
          not: req.token.iid,
        },
      },
    })
    await prisma.account.deleteMany({
      where: {
        id: {
          not: req.token.iid,
        },
      },
    })
    res.sendStatus(200)
  }
)

/**
 *
 * * Argon2 hasihing will be used
 *
 * * Resources for auth:
 * - https://softwareontheroad.com/nodejs-jwt-authentication-oauth/
 * - https://www.toptal.com/nodejs/secure-rest-api-in-nodejs
 * - https://github.com/ranisalt/node-argon2/wiki/Options
 */
