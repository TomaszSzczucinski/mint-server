/**
 *
 *  Auth.ts
 *
 *  Authorization routes
 *
 */
import { Router } from 'express'
import uploadImage from '../../middlewares/dev/middlewares.dev.multer'
import validateSchema from '../../middlewares/validation/schemaValidation'

// Mynth
import * as schemas from '../../schemas/me/schemas.me.me'
import * as tokens from '../../controllers/auth/controllers.auth.tokens'
import * as controllerMeMe from '../../controllers/me/controllers.me.me'
import * as controllerMeEdit from '../../controllers/me/controllers.me.edit'

// Export Router
export const meRouter = Router()

/**
 * Me route
 * 
 * Get profile for authentciated user
 */
meRouter.get(
  '/',
  tokens.authorizeAccessToken,
  controllerMeMe.getProfile
)

/**
 * Edit Profile
 * 
 * Edit profile for authentciated user
 */
meRouter.post(
  '/',
  tokens.authorizeAccessToken,
  validateSchema(schemas.editProfile),
  controllerMeEdit.editProfile
)

/**
 * Edit Avatar
 * 
 * Edit Profile Avatar
 */
meRouter.post(
  '/avatar',
  tokens.authorizeAccessToken,
  uploadImage.single('image'),
  controllerMeEdit.editAvatar
)