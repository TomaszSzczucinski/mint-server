/**
 *
 *  Auth.ts
 *
 *  Authorization routes
 *
 */
import { Router, Request, Response } from 'express'

// Mynth
import * as tokens from '../../controllers/auth/controllers.auth.tokens'
import * as blogController from '../../controllers/dev/controllers.dev.blog'
import * as profileController from '../../controllers/dev/controllers.dev.profile'
import * as searchController from '../../controllers/dev/controllers.dev.search'
import validateSchema from '../../middlewares/validation/schemaValidation'
import * as schemas from '../../schemas/dev/schemas.blog.blog'
import uploadImage from '../../middlewares/dev/middlewares.dev.multer'
import authenticatePostRights from '../../middlewares/dev/middlewares.dev.helpers'

// Export Router
export const devRouter = Router()

/**
 * BLOG
 */

/**
 * Get Posts
 * => Send posts
 */
devRouter.get('/blog', blogController.getPosts)

/**
 * Get Post
 * => Send post
 */
devRouter.get('/blog/:postSlug', blogController.getPost)

/**
 * Like Post
 * => Send status
 */
devRouter.get(
  '/blog/:postId/like',
  tokens.authorizeAccessToken,
  blogController.likePost
)

/**
 * Un Like Post
 * => Send status
 */
devRouter.get(
  '/blog/:postId/unlike',
  tokens.authorizeAccessToken,
  blogController.unLikePost
)

/**
 * PROFILE
 */

/**
 * Get Public Profile
 * => Send posts
 */
devRouter.get('/profiles/:profileSlug', profileController.publicProfile)

/**
 * Get Profile
 * => Send posts
 */
devRouter.get('/profile', blogController.getPosts)

/**
 * Get profile combo
 * => Send profile combo
 * Usually used to get all data at login
 */
devRouter.get(
  '/profile/combo',
  tokens.authorizeAccessToken,
  profileController.getCombo
)

/**
 * Edit Profile
 * => Send Status
 */
devRouter.post(
  '/profile',
  tokens.authorizeAccessToken,
  validateSchema(schemas.editProfile),
  profileController.editDraft
)

/**
 * Update Avatar
 * => Send Status
 */
devRouter.post(
  '/profile/avatar',
  tokens.authorizeAccessToken,
  uploadImage.single('image'),
  profileController.profileAvatar
)

/**
 * New draft
 * => Create new draft
 * => Send draft ID
 */
devRouter.post(
  '/profile/draft',
  tokens.authorizeAccessToken,
  profileController.newDraft
)

/**
 * Get draft
 * => Send draft
 */
devRouter.get(
  '/profile/draft/:draftId',
  tokens.authorizeAccessToken,
  authenticatePostRights,
  authenticatePostRights,
  profileController.getDraft
)

/**
 * Edit draft
 * => Send Status
 */
devRouter.post(
  '/profile/draft/:draftId',
  tokens.authorizeAccessToken,
  authenticatePostRights,
  validateSchema(schemas.editPost),
  profileController.editDraft
)

/**
 * Upload photo
 * => Send Status
 */
devRouter.post(
  '/profile/draft/:draftId/upload',
  tokens.authorizeAccessToken,
  authenticatePostRights,
  uploadImage.single('image'),
  profileController.uploadImage
)

/**
 * Publish draft
 * => Send Status
 */
devRouter.post(
  '/profile/draft/:draftId/publish',
  tokens.authorizeAccessToken,
  authenticatePostRights,
  profileController.publishDraft
)

/**
 * Remove draft
 * => Send Status
 */
devRouter.delete(
  '/profile/draft/:draftId',
  tokens.authorizeAccessToken,
  authenticatePostRights,
  profileController.removeDraft
)

/**
 * Edit Post
 * => Send Status
 */
devRouter.post(
  '/profile/post/:postId',
  tokens.authorizeAccessToken,
  profileController.editPost
)

/**
 * Remove Post
 * => Send Status
 */
devRouter.delete(
  '/profile/post/:postId',
  tokens.authorizeAccessToken,
  validateSchema(schemas.removePost),
  profileController.removePost
)

/**
 * Remove Post
 * => Send Status
 */
devRouter.post(
  '/profile/save/post/:postId',
  tokens.authorizeAccessToken,
  profileController.postSaveToLibrary
)

/**
 * Remove post from library
 * => Send Status
 */
devRouter.delete(
  '/profile/save/post/:postId',
  tokens.authorizeAccessToken,
  profileController.postRemoveFromLibrary
)

/**
 * SEARCH
 */

/**
 * Search
 * => Send results
 */
devRouter.get('/search', searchController.search)

/**
 * TODO Authorized search
 * - User is logged in
 *
 * => Send posts
 */
devRouter.get('/search', searchController.search)

/**
 *
 *
 *  DEV
 *
 *
 *
 */
/************************************************************************************
 *
 * Dev routes
 *
 * Dev helpers
 */
devRouter.get('/dev', async (req: Request, res: Response) => {
  res.sendStatus(200)
})
