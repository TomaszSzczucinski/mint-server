/**
 * Mynth multer middleware
 */

import multer from 'multer'

// Config
import devConfig from '../../config/dev'

const uploadImage = multer({
  dest: devConfig.upload_pre,
  limits: {
    fileSize: devConfig.avatar_max_size,
  },
  fileFilter: (_, file, cb) => {
    if (file.mimetype.startsWith('image')) {
      cb(null, true)
    } else {
      cb(null, false)
    }
  },
})

// Export middleware
export default uploadImage
