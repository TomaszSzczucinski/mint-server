/**
 * Mynth multer middleware
 */

import { PrismaClient } from '@prisma/client'
import { Request, Response, NextFunction } from 'express'

// Config
import ErrorError from '../../error/errorError'

// Init Prisma client
const prisma = new PrismaClient()

/**
 * Validate Post rights
 * @param req Request
 * @param res Response
 * @param next Next Function
 */
const authenticatePostRights = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {

  await prisma.postDraft
    .count({
      where: {
        id: Number(req.params.draftId),
        profileId: req.token.ai,
      },
    })
    .then((result) => {
      if (result !== 0) next()
      else
        throw new ErrorError(
          `${req.token.ai} no access to post ${req.params.postId}`,
          401
        )
    })
    .catch((e) => next(new ErrorError(e, 500)))
}

// Export middleware
export default authenticatePostRights
