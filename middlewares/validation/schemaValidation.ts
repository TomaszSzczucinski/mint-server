/**
 * Schema Validation
 *
 * Validation middlewares that uses
 * Joi schema validation
 */
import { Request, Response, NextFunction } from 'express'
import Joi from 'joi'

// Mynth Error
import ErrorError from '../../error/errorError'

/**
 * Validate request using sign up schema
 * @param req Request
 * @param _res _Response
 * @param next Next Function
 */
const validateSchema = (schema: Joi.ObjectSchema) => {
  return async (
    req: Request,
    _res: Response,
    next: NextFunction
  ): Promise<void> => {
    schema
      .validateAsync(req.body)
      .then(() => next())
      .catch((e) => {
        next(new ErrorError(e, 400, e.details[0].message))
      })
  }
}

export default validateSchema
