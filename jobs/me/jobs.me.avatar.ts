import Bull from 'bull'
import fs from 'fs'
import sharp from 'sharp'
import { logger } from '../../logging/winston'
import bullOpts from '../../helpers/bullRedis'


const avatarQueue = new Bull('Avatar process', {
  ...bullOpts,
  limiter: { max: 1000, duration: 5000 },
})

avatarQueue.process(async (job) => {
  const { filePath, directory } = job.data
  logger.info('Avatar processing')

  try {
    if (!fs.existsSync(directory)) fs.mkdirSync(directory)
  } catch (e) {
    logger.error('Create direcotry error', e)
    job.retry()
  }

  await Promise.all([
    sharp(filePath)
      .resize(100, 100, {
        kernel: sharp.kernel.nearest,
        fit: 'cover',
      })
      .toFile(directory + 'avatar' + '.png'),
    sharp(filePath)
      .resize(400, 400, {
        kernel: sharp.kernel.nearest,
        fit: 'cover',
      })
      .toFile(directory + 'avatar' + '.webp'),
  ])
    .then(() => {
      fs.unlink(filePath, (err) => {
        if (err) job.retry()
      })
    })
    .catch(() => job.retry())
})

export default avatarQueue
