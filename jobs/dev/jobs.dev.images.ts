import Bull from 'bull'
import bullOpts from '../../helpers/bullRedis'
import sharp from 'sharp'
import fs from 'fs'
import devConfig from '../../config/dev'
import { logger } from '../../logging/winston'

export { avatarQueue, imageQueue }

const avatarQueue = new Bull('avatar processing', {
  ...bullOpts,
  limiter: { max: 1000, duration: 5000 },
})

avatarQueue.process(async (job) => {
  const { profileId, filePath } = job.data
  logger.info('Running image avatar job')

  const directory = devConfig.upload_root + profileId + '/'

  try {
    if (!fs.existsSync(directory)) fs.mkdirSync(directory)
  } catch (e) {
    job.retry()
  }

  await Promise.all([
    sharp(filePath)
      .resize(100, 100, {
        kernel: sharp.kernel.nearest,
        fit: 'cover',
      })
      .toFile(directory + 'avatar' + '.png'),
    sharp(filePath)
      .resize(400, 400, {
        kernel: sharp.kernel.nearest,
        fit: 'cover',
      })
      .toFile(directory + 'avatar' + '.webp'),
  ])
    .then(() => {
      fs.unlink(filePath, (err) => {
        if (err) job.moveToFailed({ message: 'Remove error: ' + filePath })
      })
      logger.debug('resizing done')
    })
    .catch(() => job.retry())
})

const imageQueue = new Bull('image processing', {
  ...bullOpts,
  limiter: { max: 5, duration: 5000 },
})

imageQueue.process(async (job) => {
  const { profileId, filePath, fileName } = job.data
  logger.info('Running image upload job')

  const directory = devConfig.upload_root + profileId + '/'

  try {
    if (!fs.existsSync(directory)) fs.mkdirSync(directory)
  } catch (e) {
    job.retry()
  }

  const sharps = []

  // Original image
  sharps.push(
    sharp(filePath)
      .toFile(`${directory}${fileName}.webp`)
  )

  for (let i = 0; i < devConfig.blog_image_sizes.length; i++) {
    const width = devConfig.blog_image_sizes[0]
    sharps.push(
      sharp(filePath)
        .resize(width)
        .toFile(`${directory}${fileName}_${i + 1}.webp`)
    )
  }

  // One JPEG for browsers without support
  sharps.push(
    sharp(filePath)
      .resize(1280, null, {
        kernel: sharp.kernel.nearest,
        fit: 'cover',
      })
      .jpeg({ quality: 60, progressive: true })
      .toFile(directory + fileName + '.jpeg')
  )

  await Promise.all(sharps)
    .then(() => {
      fs.unlink(filePath, (err) => {
        if (err) job.moveToFailed({ message: 'Remove error: ' + filePath })
      })
      logger.debug('resizing done')
    })
    .catch(() => job.retry())
})
