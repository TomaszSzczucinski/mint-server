import Bull from 'bull'
import bullOpts from '../../helpers/bullRedis'
import ejs from 'ejs'
import transporter from '../../helpers/mailer'
import { logger } from '../../logging/winston'
import { PrismaClient } from '@prisma/client'

const emailQueue = new Bull('email delivery', {
  ...bullOpts,
  limiter: { max: 250, duration: 5000 },
})

emailQueue.process(async (job) => {
  // Get data
  const { type, opts } = job.data

  // Init Prisma client
  const prisma = new PrismaClient()

  // Get email template from DB
  const template = await prisma.emailTemplate.findFirst({
    where: {
      type: type,
    },
  }).catch(() => job.retry())

  // Render HTML
  if (template) {
    const html = ejs.render(template.data, opts)
    const subject = ejs.render(template.subject, opts)
    logger.info('Auth Email Queue')

    // Send email
    await transporter
      .sendMail({
        from: template.from,
        to: opts.to,
        subject: subject,
        html: html,
      })
      .catch((e) => {
        logger.error('Auth Email Queue', e)
        job.retry()
      })
  } else job.retry()
})

export default emailQueue
