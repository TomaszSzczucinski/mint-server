import Queue from 'bull'
import bullOpts from '../../helpers/bullRedis'
import transporter from '../../helpers/mailer'
import { logger } from '../../logging/winston'

import * as emailTemplates from '../../helpers/emails/helpers.email.sign'

export { emailQueue }

const emailQueue = new Queue('email delivery old', {
  ...bullOpts,
  limiter: { max: 250, duration: 5000 },
})

emailQueue.process(async (job) => {
  const { type, to, vars } = job.data

  let message = null

  switch (type) {
  case 'welcome':
    message = await emailTemplates
      .welcome(to, vars)
      .catch((e) => console.log(e))
    break
  case 'resetPassword':
    message = await emailTemplates
      .resetPassword(to, vars)
      .catch((e) => logger.error('message', e))
    break
  case 'sendPin':
    message = await emailTemplates
      .sendPin(to, vars)
      .catch((e) => console.log(e))
    break
  default:
    break
  }
  logger.info(message)

  if (!message) job.retry()
  else
  {
    await transporter.sendMail(message).catch((e) => {
      logger.error('emailQueue.process', e)
    })
  }
})
