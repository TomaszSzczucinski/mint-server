export default {
  /**
   * Winston logger formating 
   */


  /**
   * Winston logger config
   */
  log_level: process.env.NODE_ENV == 'development' ? 'debug' : 'info',

  log_directory: 'logs',
  log_file: 'mint-server.log',
  requests_log_file: 'mint-server-requests.log',

}