/**
 * Main config file
 */
import dotenv from 'dotenv'

const envFound = dotenv.config()

if (envFound.error)
{
  // Throw error if .env file isn't available
  throw new Error('Can\'t find .env file!')
}

export default {
  /**
   * Port
   * * Default is `3000`
   */
  port: 3000,

  /**
   * Session
   */
  session_secret: `${process.env.SESSION_SECRET}`,

  /**
   * JWT
   * !JWT_TOKENS should never be published anywhere!!!
   * ? Should they be here???
   * ? I dont't won't them to be read into memory variable
   */
  jwt_acess_token_secret: `${process.env.ACCESS_TOKEN_SECRET}`,
  jwt_access_token_life: `${process.env.ACESS_TOKEN_LIFE}`,

  jwt_refresh_token_secret: `${process.env.REFRESH_TOKEN_SECRET}`.toString(),
  jwt_refresh_token_life: `${process.env.REFRESH_TOKEN_LIFE}`.toString(),

  /**
  * Morgan logger settings
  */
  morgan_format: process.env.NODE_ENV == 'development' ? 'dev' : 'combined',

  /**
   * Redis config
   */
  redis_port: parseInt(`${process.env.REDIS_PORT}`) || 6379,
  redis_secret: process.env.REDIS_SECRET,

  /**
  * API prefixes
  */
  api_prefix: 'api',
  api_auth_prefix: 'auth',

  mail_pass: process.env.MAIL_PASS,

}