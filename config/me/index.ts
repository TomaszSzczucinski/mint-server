export default {
  upload_root: '/var/www/media/me/',
  upload_pre: '/var/www/media/me/processing',
  media_path: 's.mynth.io/me/',
}