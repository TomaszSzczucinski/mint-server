/**
 * 
 *  Mynth Blog Configuration
 *  Updated:
 * 
 */

export default {
  upload_root: '/var/www/media/dev/',
  upload_pre: '/var/www/media/dev/processing',
  blog_image_sizes: [400],
  avatar_max_size: 1024 * 1024 * 10, // 10 MB
  min_title_len: 5,
  media_path: 's.mynth.io/dev/',
  img_vars: ['.webp', '.jpeg', '-1.webp'],
  media_limit: 150,
  default_limit: 36
}