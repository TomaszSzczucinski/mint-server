export default {
  /**
   * WhiteBlack Request settings
   */
  validForDays: 30, // Days
  undefinedValidForDays: 1, // Days

  /**
   * Rate limitter settings
   */
  maxWrongAttemptsByIPperDay: 100,
  maxConsecutiveFailsByUsernameAndIP: 10,

  /**
   * Reset Password
   */
  resetTokenExpireTime: 30 // Minutes
}