/**
 * Interfaces for response messaging
 */


// Keep it simple
export interface ErrorErrorMessageInterface {
  sucess: boolean,
  type: string,
  message: string,
  element?: string
}