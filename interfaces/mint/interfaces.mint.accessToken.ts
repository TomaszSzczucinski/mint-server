/**
 * Interfaces for response messaging
 */

// Keep it simple
export interface AccessTokenInterface {
  iid: number
  irl: string[]
  pid: number,
  iat: number,
  exp: number
}
