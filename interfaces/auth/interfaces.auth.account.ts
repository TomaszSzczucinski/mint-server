/**
 * Account object
 */

export interface accountInterface {
  id: string,
  name: string
  email: string
  password?: string
  role: string[]
  profileID: string
  createdAt?: Date
  updatedAt?: Date
}

/**
 * White & Black interface
 */
export interface AuthRequestInterface {
  id: string,
  _id: string,
  ipAddress: string,
  browserName: string,
  browserOs: string,
  validTo: Date,
  pin: number,
  black: boolean,
}