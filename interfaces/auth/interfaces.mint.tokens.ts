/**
 * TODO Interfaces for response messaging
 */

export interface AccessTokenInterface {
  iid: number
  irl: string[]
  pid: number,
  iat: number,
  exp: number
}

export interface RefreshTokenInterface {
  iid: number
  iip: string,
  iua: string,
  rid: number,
  iat: number,
  exp: number
}
