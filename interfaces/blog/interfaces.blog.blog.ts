/**
 * Mynth
 *
 * Blog interfaces
 */

export interface PostUpdateInterface {
  title?: string
  tags?: string[]
  authors?: number[]
  hlights?: any[]
}

export interface PublicPostInterface {
  title: string
  imageUrl: string | null
  content: JSON
  hlights: JSON
  tags: string[]
}
