/**
 *
 *  Mynth dev - Profile controller
 *
 */
import { Request, Response, NextFunction } from 'express'
import fs from 'fs'

// Mynth config
import devConfig from '../../config/dev'

// Mynth Error
import ErrorError from '../../error/errorError'

// Mynth Services
import * as servicesProfile from '../../services/dev/services.dev.profile'
import { avatarQueue, imageQueue } from '../../jobs/dev/jobs.dev.images'
import { logger } from '../../logging/winston'

/**
 * Exports
 */
export {
  publicProfile,
  getCombo,
  edit,
  profileAvatar,
  newDraft,
  getDraft,
  editDraft,
  removeDraft,
  removePost,
  publishDraft,
  editPost,
  postSaveToLibrary,
  postRemoveFromLibrary,
  uploadImage,
}

/**
 * Get Draft
 * @param req Request
 * @param res Response
 * @param next Next Function
 */
const getDraft = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  // Get draft ID
  const draftId = Number(req.params.draftId)

  // Edit draft
  servicesProfile
    .getDraft(draftId)
    .then((data) => {
      if (data) res.status(200).json(data)
      else res.sendStatus(400)
    })
    .catch((e) => next(new ErrorError(e, 500)))
}

/**
 * Edit Draft
 * @param req Request
 * @param res Response
 * @param next Next Function
 */
const editDraft = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  // Get draft ID
  const draftId = Number(req.params.draftId)

  // Edit draft
  servicesProfile
    .editDraft(draftId, req.body)
    .then((success) => {
      if (success) res.sendStatus(200)
      else res.sendStatus(400)
    })
    .catch((e) => next(new ErrorError(e, 500)))
}

/**
 * Remove draft
 * @param req Request
 * @param res Response
 */
const removeDraft = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  // Always by slug
  const draftId = Number(req.params.draftId)

  const success = await servicesProfile
    .removeDraft(draftId)
    .catch((e) => next(new ErrorError(e, 400)))

  if (success) res.sendStatus(200)
  else res.sendStatus(400)
}

/**
 * Edit post
 * @param req Request
 * @param res Response
 */
const editPost = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  // Always by slug
  const postId = Number(req.params.postId)

  await servicesProfile
    .draftFromPost(req.token.ai, postId)
    .then((draft) => {
      if (draft) res.status(200).json(draft.id)
      else res.sendStatus(400)
    })
    .catch((e) => next(new ErrorError(e, 400)))
}

/**
 * Remove draft
 * @param req Request
 * @param res Response
 */
const removePost = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  // Always by slug
  const postId = Number(req.params.postId)

  const success = await servicesProfile
    .removePost(req.token.ai, postId, req.body.saveDraft)
    .catch((e) => next(new ErrorError(e, 400)))

  if (success) res.sendStatus(200)
  else res.sendStatus(400)
}

/**
 * Edit Profile
 * @param req Request
 * @param res Response
 * @param next Next Function
 */
const publicProfile = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  // Get combo
  servicesProfile
    .publicProfile(req.params.profileSlug)
    .then((data) => {
      if (data) res.status(200).json(data)
      else res.sendStatus(404)
    })
    .catch((e) => next(new ErrorError(e, 500)))
}

/**
 * Edit Profile
 * @param req Request
 * @param res Response
 */
const getCombo = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  logger.info('Combo')
  // Get combo
  servicesProfile
    .combo(req.token.ai)
    .then(async (data) => {
      if (data) res.status(200).json(data)
      else {
        servicesProfile.createDevProfile(req.token.ai).then((data) => {
          if (data) res.status(200).json(data)
          else res.sendStatus(400)
        })
      }
    })
    .catch((e) => {
      logger.debug(e)
      next(new ErrorError(e, 500))
    })
}

/**
 * Edit Profile
 * @param req Request
 * @param res Response
 */
const edit = async (req: Request, res: Response): Promise<void> => {
  res.sendStatus(200)
}

/**
 * Update avatar
 * @param req  Request
 * @param res  Response
 * @param next Next Function
 */
const profileAvatar = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  console.log(req.file)

  await servicesProfile
    .addAvatar(req.token.ai)
    .then(async () => {
      avatarQueue
        .add(
          { profileId: req.token.ai, filePath: req.file.path },
          {
            attempts: 100,
            backoff: 5000,
          }
        )
        .then(() => res.sendStatus(200))
        .catch((e) => next(new ErrorError(e, 500)))
    })
    .catch((e) => next(new ErrorError(e, 500)))
}

/**
 * Upload draft image
 * @param req  Request
 * @param res  Response
 * @param next Next Function
 */
const uploadImage = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  await servicesProfile
    .addImage(req.token.ai, req.file.filename)
    .then(async (success) => {
      if (!success) next(new ErrorError('Add image failed', 400))
      else {
        imageQueue
          .add(
            {
              profileId: req.token.ai,
              filePath: req.file.path,
              fileName: req.file.filename,
            },
            {
              attempts: 1,
              backoff: 5000,
            }
          )
          .then(() => {
            res.status(200).json({
              file_name: req.file.filename,
              media_path:
                devConfig.media_path + req.token.ai + '/' + req.file.filename,
              img_vars: devConfig.img_vars,
            })
          })
          .catch((e) => {
            fs.unlink(req.file.path, (err) => logger.error('Unlink image', err))
            next(new ErrorError(e, 500))
          })
      }
    })
    .catch((e) => next(new ErrorError(e, 500)))
}

/**
 * New draft
 * @param req   Request
 * @param res   Response
 * @param next  NextFunction
 */
const newDraft = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  await servicesProfile
    .createDraft(req.token.ai)
    .then((draft) => {
      res.status(200).json(draft)
    })
    .catch((e) => next(new ErrorError(e, 500)))
}

/**
 * Update avatar
 * @param req Request
 * @param res Response
 */
const publishDraft = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  // Get draft ID
  const draftId = Number(req.params.draftId)

  // Publish
  await servicesProfile
    .publishDraft(draftId, req.token.ai)
    .then((result) => {
      if (result) res.sendStatus(200)
      else res.sendStatus(400)
    })
    .catch((e) => next(new ErrorError(e, 500)))
}

/**
 * Save post to library
 * @param req   Request
 * @param res   Response
 * @param next  Next Function
 */
const postSaveToLibrary = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  // Get post ID
  const postId = Number(req.params.postId)

  // Publish
  await servicesProfile
    .addPostToLibrary(req.token.ai, postId)
    .then((success) => {
      if (success) res.sendStatus(200)
      else res.sendStatus(400)
    })
    .catch((e) => next(new ErrorError(e, 500)))
}

/**
 * Remove post from library
 * @param req   Request
 * @param res   Response
 * @param next  Next Function
 */
const postRemoveFromLibrary = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  // Get post ID
  const postId = Number(req.params.postId)

  // Publish
  await servicesProfile
    .removePostFromLibrary(req.token.ai, postId)
    .then((success) => {
      if (success) res.sendStatus(200)
      else res.sendStatus(400)
    })
    .catch((e) => next(new ErrorError(e, 500)))
}
