/**
 *
 *  Mynth dev - search controller
 *
 */
import { Request, Response, NextFunction } from 'express'

// Mynth Error
import ErrorError from '../../error/errorError'

// Mynth Services
import * as servicesSearch from '../../services/dev/services.dev.search'

/**
 * Exports
 */
export { search, authSearch }

/**
 * Search
 *
 * Standard search
 *
 * @param req Request
 * @param res Response
 * @param next Next Function
 */
const search = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  const searchFor = req.query.s

  if (typeof searchFor !== 'string') throw new ErrorError('No string passed to search', 400)

  Promise.all([
    servicesSearch.searchPosts(searchFor),
    servicesSearch.searchProfiles(searchFor),
  ])
    .then(([posts, profiles]) => {
      res.status(200).json({
        posts: posts,
        profiles: profiles
      })
    })
    .catch((e) => next(new ErrorError(e, 500)))
}

/**
 * Authorized Search
 *
 * Search also in user profile
 *
 * @param req Request
 * @param res Response
 * @param next Next Function
 */
const authSearch = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  const searchFor = req.query.s

  if (typeof searchFor !== 'string') throw new ErrorError('No string passed to search', 400)

  Promise.all([
    servicesSearch.searchPosts(searchFor),
    servicesSearch.searchProfiles(searchFor),
  ])
    .then(([posts, profiles]) => {
      res.status(200).json({
        posts: posts,
        profiles: profiles
      })
    })
    .catch((e) => next(new ErrorError(e, 500)))
}
