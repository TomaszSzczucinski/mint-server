/**
 *
 *  Mynth blog controller
 *
 */
import { Request, Response, NextFunction } from 'express'

// Mynth Error
import ErrorError from '../../error/errorError'

// Mynth Services
import * as servicesBlog from '../../services/dev/services.dev.blog'
import { checkLike } from '../../services/dev/services.dev.profile'

/**
 * Exports
 */
export { getPosts, getPost, likePost, unLikePost }

/**
 * Return posts
 * @param req Request
 * @param res Response
 * @param next Next Function
 */
const getPosts = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  // Get filters and options
  const categories = req.queryPolluted.c || [req.query.c || null]
  const pLangs = req.queryPolluted.p || [req.query.p || null]
  const page = Number(req.query.page) || 0
  const take = Number(req.query.limit)

  servicesBlog
    .getPosts(categories, pLangs, page, take)
    .then((posts) => {
      res.status(200).json({
        posts: posts,
      })
    })
    .catch((e) => next(new ErrorError(e, 500)))
}

/**
 * Send single post
 * @param req Request
 * @param res Response
 * @param next Next Function
 */
const getPost = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  // Get slug
  const postSlug = req.params.postSlug

  // Check if slug exists
  if (!postSlug) {
    res.sendStatus(404)
    return
  }

  // Send Post
  servicesBlog
    .getPostBySlug(postSlug)
    .then((post) => {
      if (post) {
        res.status(200).json({
          post,
        })
      } else {
        res.sendStatus(404)
      }
    })
    .catch((e) => next(new ErrorError(e, 500)))
}

/**
 * Like post and send ok if liked
 * @param req Request
 * @param res Response
 * @param next Next Function
 */
const likePost = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  // Always by slug
  const postId = Number(req.params.postId)

  // Get reactions for post and profile
  checkLike(req.token.ai, postId)
    .then(async (isLiked) => {
      if (isLiked) res.sendStatus(200)
      else {
        await servicesBlog.like(req.token.ai, postId)
        res.sendStatus(200)
      }
    })
    .catch((e) => next(new ErrorError(e, 500)))
}

/**
 * Un Like post and send ok if liked
 * @param req Request
 * @param res Response
 * @param next Next Function
 */
const unLikePost = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  // Always by slug
  const postId = Number(req.params.postId)

  checkLike(req.token.ai, postId)
    .then(async (isLiked) => {
      if (isLiked) {
        await servicesBlog.unLike(req.token.ai, postId)
        res.sendStatus(200)
      } else res.sendStatus(200)
    })
    .catch((e) => next(new ErrorError(e, 500)))
}
