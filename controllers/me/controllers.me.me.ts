import { Request, Response, NextFunction } from 'express'

import * as serviceMeMe from '../../services/me/services.me.me'

export { getProfile }

/**
 * Return posts
 * @param req Request
 * @param res Response
 * @param next Next Function
 */
const getProfile = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  const profile = await serviceMeMe.getProfile(req.token.ai)
  if (profile) res.status(200).json({profile: profile})
  else res.sendStatus(500)
}
