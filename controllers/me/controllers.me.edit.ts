import { Request, Response, NextFunction } from 'express'
import ErrorError from '../../error/errorError'

import * as serviceMeEdit from '../../services/me/services.me.edit'

export { editProfile, editAvatar }

/**
 * Return posts
 * @param req Request
 * @param res Response
 * @param next Next Function
 */
const editProfile = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  const profile = await serviceMeEdit.editProfile(req.token.ai, req.body).catch(e => next(new ErrorError(e)))
  if (profile) res.status(200).json({profile: profile})
  else res.sendStatus(500)
}

/**
 * Edit avatar
 * @param req Request
 * @param res Response
 * @param next Next Function
 */
const editAvatar = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  const avatarUrl = await serviceMeEdit.processAvatar(req.token.ai, req.file.path).catch(e => next(new ErrorError('Image processing', e)))
  const profile = await serviceMeEdit.editProfile(req.token.ai, {avatar: avatarUrl}).catch(e => next(new ErrorError(e)))
  if (profile) res.status(200).json({profile: profile})
  else res.sendStatus(500)
}
