// /**
//  * Tokens controllers
//  */
import { Request, Response, NextFunction } from 'express'
import { PrismaClient } from '@prisma/client'
import jwt from 'jsonwebtoken'
import ErrorError from '../../error/errorError'
import {
  extractAccessToken,
  generateAccessToken,
  generateRefreshToken,
  authorizeRefreshToken,
} from '../../services/auth/services.auth.tokens'
import { logger } from '../../logging/winston'

export { authorizeAccessToken, refreshAccessToken, sendAccessToken }

// Init Prisma client
const prisma = new PrismaClient()

/**
 * Response with Access Token
 * @param req Request
 * @param res Response
 * @param next Next Function
 */
const sendAccessToken = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  if (req.account) {
    // Check if we need to redirect user to last vieved page
    const goBackTo = req.query.gto
      ? decodeURIComponent(`${req.query.gto}`)
      : 'mynth.io/me'
    // Generate Access Token
    const accessToken = await generateAccessToken(req.account).catch((e) =>
      next(e)
    )

    // res.cookie('at', accessToken, {secure: true, httpOnly: true})
    res.cookie('at', accessToken, {signed: true, httpOnly: true})
    // Response with success, acces token and redirect if any
    res.sendStatus(200)
  } else res.sendStatus(401)
}

/**
 * Check validity of access token
 * Set token in 'req'.
 * @param req Request
 * @param res Response
 * @param next Next Function
 */
const authorizeAccessToken = async (
  req: Request,
  _res: Response,
  next: NextFunction
): Promise<void> => {

  // Extract token from request
  await extractAccessToken(req)
    .then((tokenString) => {
      try {
        // Verify Token
        const token:any = jwt.verify(
          tokenString,
          `${process.env.ACCESS_TOKEN_SECRET}`
        )

        // Validate ii
        if (token)
        {
          // Set token and pass to next function
          req.token = token
          next()
        }
        else
        {
          throw new Error()
        }
      } catch (err) {
        throw new ErrorError(
          'authorizeAccessToken',
          401,
          'Brak dostępu',
        )
      }
    })
    .catch((err) => next(err))
}

/**
 * Validate refresh token
 * Get account set in req and call
 * next function which is send acces token
 * @param req Request
 * @param res Response
 * @param next Next function
 */
const refreshAccessToken = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  // Get user agent
  const ipAddress = req.ip
  const userAgent = req.headers['user-agent'] || 'undefined/undefined'

  // Authorize refresh tokek and get token data
  //  Also if issuer ip changed, changes ip in
  //  connected authorized request
  await authorizeRefreshToken(req.session.rt, ipAddress, userAgent)
    .then(async (refreshTokenData) => {
      prisma.account
        .findUnique({
          where: { id: refreshTokenData.ai },          
        })
        .then(async (account) => {
          // Throw error if account not found
          // FUTURE:  && account.blocked === false
          if (account) {         
            // If we need to generate refresh token again...   
            if (refreshTokenData.newRefreshToken) {
              req.session.rt = await generateRefreshToken(
                account,
                refreshTokenData.ri,
                ipAddress,
                userAgent
              )
            }
            // Set account in req so we can use it in next function
            // to send access token
            req.account = account
            next()
          } else throw new ErrorError('refreshAccessToken', 500)
        })
        .catch((e) => {
          logger.warn('Prisma client', e)
          next(new ErrorError('Prisma', 401))
        })
    })
    .catch((e) => {
      next(e)
    })
}
