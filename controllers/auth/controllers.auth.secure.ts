/**
 * Secure controller
 */
import { Request, Response, NextFunction } from 'express'
import ErrorError from '../../error/errorError'
import { logger } from '../../logging/winston'

import * as servicesSecure from '../../services/auth/services.auth.secure'

export { allowedFor, listAuthRequests, unblockAuthRequest, removeAuthRequest }

/**
 * Check if user has given role and can access targeting endpoint
 * @param role Role that should be allowed for
 */
const allowedFor = (role: string) => {
  return async (
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> => {
    // God has access everywhere
    if (req.token.rl === role || req.token.rl === 'God') {
      next()
    } else {
      logger.warn('Unauthorized Access', req.token)
      res.sendStatus(401)
    }
  }
}

/**
 * Validate refresh token
 * Get account set in req and call
 * next function which is send acces token
 * @param req Request
 * @param res Response
 * @param next Next function
 */
const listAuthRequests = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  await servicesSecure.getAuthRequests(req.token.ai)
    .then((requests) => {
      res.status(200).json(requests)
    })
    .catch((e) => next(new ErrorError(e, 500)))
}

/**
 * Validate refresh token
 * Get account set in req and call
 * next function which is send acces token
 * @param req Request
 * @param res Response
 * @param next Next function
 */
const unblockAuthRequest = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  // Get request id
  const requestId = Number(req.params.id)

  await servicesSecure.unblockAuthRequest(req.token.ai, requestId)
    .then(() => {
      res.sendStatus(200)
    })
    .catch((e) => next(new ErrorError(e, 500)))
}

/**
 * Validate refresh token
 * Get account set in req and call
 * next function which is send acces token
 * @param req Request
 * @param res Response
 * @param next Next function
 */
const removeAuthRequest = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  // Get request id
  const requestId = Number(req.params.id)

  await servicesSecure.removeRequest(req.token.ai, requestId)
    .then((result) => {
      if (result) res.sendStatus(200)
      else res.sendStatus(400)
    })
    .catch((e) => next(new ErrorError(e, 500)))
}
