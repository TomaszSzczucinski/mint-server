/**
 * Mint User controller
 *
 */
import { Request, Response, NextFunction } from 'express'
import * as argon2 from 'argon2'

import {
  createAccount,
  authenticateAccount,
  addResetPasswordToken,
  validateResetPasswordToken,
  updatePasswordWithWipe,
  updatePassword,
  removeAccount,
  getAccountPassword,
} from '../../services/auth/services.auth.account'
import * as secureSign from '../../services/auth/services.auth.secure'
import * as authTokens from '../../services/auth/services.auth.tokens'
import { emailQueue } from '../../jobs/auth/jobs.auth.shot'
import ErrorError from '../../error/errorError'
import { logger } from '../../logging/winston'

/**
 * Export
 */
export {
  signIn,
  signUp,
  signOut,
  issuePasswordReset,
  passwordReset,
  setNewPassword,
  wipeAccount,
}

/**
 * Queue's
 */

/**
 * Sign in, validate account and password
 * @param req Request
 * @param res Response
 * @param next Next Function
 */
const signIn = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  // Get data from body
  const email = req.body.email
  const password = req.body.password
  const pin = req.body.pin || null

  // Get user agent
  const ipAddress = req.ip
  const userAgent = req.headers['user-agent'] || 'undefined/undefined'

  // Check if user is already logged in
  if (
    req.session.rt &&
    (await authTokens
      .authorizeRefreshToken(req.session.rt, ipAddress, userAgent)
      .catch(() => {
        return false
      }))
  ) {
    res.sendStatus(200)
    return
  }
  // Authenticate account
  await authenticateAccount(email, password, ipAddress, userAgent)
    .then(async (account) => {
      // Do tasks to secure sign in even if data is OK
      // Authenticated account needs to be checked
      // Ipaddress and user agent needs to be matched,
      // otherwise we need to verify login with pin
      // VPN won't mean anything here
      const isRequestAuthorized = await secureSign
        .isAuthorizedRequest(
          account,
          account.authRequest[0],
          ipAddress,
          userAgent,
          pin
        )
        .catch((err) => {
          err.message = 'Wystąpił problem z logowaniem'
          throw err
        })

      console.log(
        '🚀 ~ file: controllers.auth.account.ts ~ line 80 ~ .then ~ isRequestAuthorized',
        isRequestAuthorized
      )

      if (isRequestAuthorized) {
        // Generate refresh token
        req.session.rt = await authTokens.generateRefreshToken(
          account,
          isRequestAuthorized.id,
          ipAddress,
          userAgent
        )

        // Set account
        req.account = account

        // Set ii
        req.session.ai = account.id

        // -> Send access token
        next()
      } else {
        // Request for new request authorization is done automaticly
        // via isRequestAuthorized function
        // Repsonse
        res.sendStatus(403)
      }
    })
    .catch((err) => {
      next(err)
    })
}

/**
 * Validate email, sign up user
 *
 * @param req Request
 * @param res Response
 * @param next Next function
 */
const signUp = async (
  req: Request,
  _res: Response,
  next: NextFunction
): Promise<void> => {
  // Get form data
  const name = req.body.name
  const email = req.body.email
  const password = req.body.password

  // Request information
  const ipAddress = req.ip
  const userAgent = req.headers['user-agent'] || 'undefined/undefined'

  // Meta informations
  // const goBackTo = req.body.meta.ifm

  // Create new account
  createAccount(name, email, password, ipAddress, userAgent)
    .then(async (account) => {
      // Remove password (just to be sure)
      account.password = ''
      // Send welcome email
      emailQueue.add(
        { type: 'welcome', to: account.email, vars: [account.name] },
        {
          attempts: 5,
          backoff: {
            type: 'exponential',
            delay: 2000,
          },
        }
      )

      // Generate refresh token
      req.session.rt = await authTokens.generateRefreshToken(
        account,
        account.authRequest[0].id,
        ipAddress,
        userAgent
      )

      // Set account for access token
      req.account = account

      // -> Send access token
      next()
    })
    .catch((err) => {
      next(err)
    })
}

/**
 * Sign out
 *
 * @param req Request
 * @param res Response
 * @param next Next function
 */
const signOut = async (req: Request, res: Response): Promise<void> => {
  req.session.rt = null
  res.clearCookie('at')
  req.session.destroy(() => {
    res.status(200).json({
      scs: true,
      gto: 'mynth.io',
    })
  })
}

/**
 * Issue Password reset
 *
 * @param req Request
 * @param res Response
 * @param next Next function
 */
const issuePasswordReset = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  // Get form data
  const email = req.body.email

  // Request information
  // It would be nice to store this somewhere in future
  // const ipAddress = req.ip
  // const userAgent = req.headers['user-agent'] || 'undefined/undefined'

  // Add reset token to account and get name to say hello in email :)
  const nameTokenPair = await addResetPasswordToken(email).catch((e) =>
    next(new ErrorError(e))
  )

  await emailQueue.add(
    { type: 'resetPassword', to: email, vars: nameTokenPair },
    {
      attempts: 20,
      backoff: {
        type: 'fixed',
        delay: 5000,
      },
    }
  ).catch(e => next(new ErrorError(e)))
  res.status(200).json('Link resetujący został wysłany na twojego maila')
}

/**
 * Reset password
 *
 * @param req Request
 * @param res Response
 * @param next Next function
 */
const passwordReset = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  // Get form data
  const token = req.body.token
  const password = req.body.password

  // Request information
  const ipAddress = req.ip
  const userAgent = req.headers['user-agent'] || 'undefined/undefined'

  // Check password reset token
  await validateResetPasswordToken(token)
    .then(async (accountId) => {
      // Secure options
      // Clear all logged in devices
      await secureSign
        .clearAuthForAccount(accountId)
        .then(async () => {
          // Update password and authorized request to the reuqest
          await updatePasswordWithWipe(
            accountId,
            password,
            ipAddress,
            userAgent
          )
            .then(() => {
              // Response
              res
                .status(200)
                .json('Hasło zresetowane, proszę się teraz zalogować')
            })
            .catch((e) => next(new ErrorError(e, 401)))
        })
        .catch((e) => next(new ErrorError(e, 401)))
    })
    .catch((e) => next(new ErrorError(e, 401)))
}

/**
 * Set new Password
 *
 * @param req Request
 * @param res Response
 * @param next Next function
 */
const setNewPassword = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  // Get data from request
  const checkPassword = req.body.ap
  const newPassword = req.body.np

  // Request information
  const ipAddress = req.ip
  const userAgent = req.headers['user-agent'] || 'undefined/undefined'

  // User needs to be logged in so id is available with ii
  await getAccountPassword(req.token.ai)
    .then(async (password) => {
      // Check if password match
      const isPasswordMatch = await argon2.verify(password, checkPassword)

      if (!isPasswordMatch)
        res.status(401).json({
          sc: false,
          mg: 'Złe aktualne hasło',
        })
      else {
        if (req.body.so === true) {
          await secureSign.clearAuthForAccount(req.token.ai)
          await updatePasswordWithWipe(
            req.token.ai,
            newPassword,
            ipAddress,
            userAgent
          )
          req.session.rt = null
          req.session.ai = null
        } else await updatePassword(req.token.ai, newPassword)

        res.sendStatus(200)
      }
    })
    .catch((e) => next(new ErrorError(e, 401)))
}

/**
 * Remove account
 *
 * @param req Request
 * @param res Response
 * @param next Next function
 */
const wipeAccount = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  removeAccount(req.token.ai)
    .then(() => {
      res.sendStatus(200)
    })
    .catch((e) => {
      logger.error(e)
      next(new ErrorError('Delete Account', 500, 'Nie można usunąć konta'))
    })
}
